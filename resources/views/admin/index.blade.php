<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Covid-19</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <script
			  src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			  crossorigin="anonymous"></script>
    </head>
    <body>

      
        <header>
          <nav class="navbar navbar-light bg-home text-center">
            <div class="mx-auto">
              <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
              </h2>           
              <a class="navbar-brand mb-0 h1 text-right" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
               {{ __('Salir') }}
           </a>

           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
               @csrf
           </form>
            </div>
          </nav>
        </header>
        <div class="container">

            <div class="row  mx-auto mt-5 ">
                <div class="mx-auto">
                    <h2 class="navbar-brand mb-0 h1 text-center">Usuarios Registrados</h2>
                  </div>
                        <div class="col-lg-4"></div>
                       
                        <div class="table-responsive">
                          <form method="post"  action="{{route('admin.users.filter')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-12 col-lg-12">
                                    <label for=""></label>
                                <div class="input-group col-12">
                                <a href="{{route('admin.users.create')}}" class="btn btn-secondary">Crear Nuevo Usuario</a>
                                    <input type="text" name="filter" id="filter" tabindex="1" class="form-control" 
                                    placeholder="Ingrese Nombre" required>
                                    <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-success ">Buscar</button>
                                </div>
                                </div>
                            </div>
                        </form>
                            <table class="table table-hover">
                                <tr>
                                    <th>Nombre</th>
                                    <th>Email</th>
                                    <th>Rol</th>
                                    <th></th>

                                </tr>
                                <tbody>
                                    @isset($users)
                                    @foreach ($users as $user)
                                       <tr style="background-color:#{{$user->triage}};">
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}} 
                                        </td>
                                        <td>
                                        @if($user->hasRole('admin'))
                                        Administrador
                                        @else
                                        Usuario
                                        @endif 
                                        </td>
                                        <td>
                                        <a class="btn btn-success" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                          document.getElementById('admin.users.edit').submit();">
                                             {{ __('Editar') }}
                                         </a>
                              
                                         <form id="admin.users.edit" action="{{ route('admin.users.edit') }}" method="POST" style="display: none;">
                                             @csrf
                                         <input type="hidden" name="id_user" value="{{$user->id}}">
                                         </form>
                                        </td>
                                        <td>
                                            <a class="btn btn-danger" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                          document.getElementById('admin.users.delete').submit();">
                                             {{ __('Borrar') }}
                                         </a>
                              
                                         <form id="admin.users.delete" action="{{ route('admin.users.delete') }}" method="POST" style="display: none;">
                                             @csrf
                                             <input type="hidden" name="id_user" value="{{$user->id}}">

                                         </form>
                                        </td>

                                        </tr>
                                    @endforeach
                                        
                                    @endisset
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination">
                          <li class="page-item"><a class="page-link" href="{{$users->previousPageUrl()}}"> atrás
                          </a></li>
                            <li class="page-item"><a class="page-link" href="#">{{$users->currentPage()}}</a></li>
    
                          <li class="page-item"><a class="page-link" href="{{$users->nextPageUrl()}}">siguiente</a></li>
                        </ul> 
                        <div class="col-lg-4">

                        </div>

            </div>

        </div>
      <!-- <a href="#" class="thumbnail btn">
        <figure>
          <img src="http://i.stack.imgur.com/g1Ce8.png" alt="bg" />
          <figcaption>
            <div>
            Caption here
            </div>
          </figcaption>
        </figure>
      </a> -->
      <script type="text/javascript">


    </script>
    </body>
</html>
