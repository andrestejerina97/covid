<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <!-- CODELAB: Add iOS meta tags and icons -->
        <meta name="apple-mobile-web-app-title" content="PWA COVID">
        <meta name="theme-color" content="#ffffff"/>
        <meta name="description" content="Una aplicación para combatir el COVID 19">

      <link rel="apple-touch-icon" href="{{asset("icon-152x152.png")}}">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />    
    <link rel="manifest" href="{{asset("manifest.json")}}" data-pwa-version="set_by_pwa.js">

        <title>Covid-19</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    
    @yield('css')
    <script
			  src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			  crossorigin="anonymous"></script>
    </head>
    <body>

      <main>
        @yield('content')
      </main>
    <script src="{{asset('scripts/custom.js')}}">

</script>
      
      @yield('scripts')

    </body>
</html>
