<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Covid-19</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <script
			  src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			  crossorigin="anonymous"></script>
    </head>
    <body>

        <div id="background" class="preloader"></div>
    <div id="logocontainer" class="preloader" onclick=fill()>
      <div id="pelogo"></div>
      
    </div>
        <header>
          <nav class="navbar navbar-light bg-home text-center">
            <div class="mx-auto">
              <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
              </h2>
                <a class="navbar-brand mb-0 h1 text-right" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Salir') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>

          </nav>
        </header>
        <div class="container">

            <div class="row   mt-5 ">
              <div class="mx-auto">
                        <div class="col-lg-4 col-md-4 col-xs-4 col-4 ">
                          <div class="d-block btn-home">
                          <a href="{{route('nuevoingreso')}}"> <button class="btn btn-success btn-lg mt-3">Nuevo ingreso<img src="{{asset('app/icons/plusM.png')}}" alt=""></button></a>

                          </div>
                          <div class="d-block btn-home">

                          <a href="{{route('search')}}"><button  class="btn btn-lg  btn-success mt-3">Buscar<img src="{{asset('app/icons/searchM.png')}}" alt=""></button></a>
                      </div>
                      <div class="d-block btn-home">
                        <a   href="{{route('charts')}}""> <button class=" btn btn-lg  btn-success mt-3"> Gráficos<img src="{{asset('app/icons/presentacion.png')}}" alt=""></button></a>

                       </div>
                       @if(@Auth::user()->hasRole('admin'))
                       <div class="d-block btn-home">
                        <a   href="{{route('admin.users')}}""> <button class=" btn btn-lg  btn-success mt-3">Administración</button></a>
                       </div>                        
                       @endif
               
  
                      </div>

            </div>
          </div>
        </div>
      <!-- <a href="#" class="thumbnail btn">
        <figure>
          <img src="http://i.stack.imgur.com/g1Ce8.png" alt="bg" />
          <figcaption>
            <div>
            Caption here
            </div>
          </figcaption>
        </figure>
      </a> -->
      <script type="text/javascript">
    var full=false;

function fill(){
  full=!full;
  document.getElementById("logocontainer").style.backgroundColor=full?"#3ebffa":"transparent";
  
}
  //After 2s preloader is fadeOut
  $('#logocontainer').delay(2000).fadeOut('slow');
    setTimeout(function() {
        //After 2s, the no-scroll class of the body will be removed
        $('#background').removeClass('preloader');
        $('#background').attr('id','2');

    }, 2000); //Here you can change preloader time
    </script>
    </body>
</html>
