@extends('layouts.public')
@section('content')
<header>
    <nav class="navbar navbar-light bg-home text-center">
        <div class="mx-auto">
            <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
            </h2>
            <a class="navbar-brand mb-0 h1 text-right" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
             {{ __('Salir') }}
         </a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
         </form>
        </div>
    </nav>
</header>
<div class="container">

    <div class="row mt-5 ml-1">
        <div class=" mx-auto">
            <form method="POST" action="{{route('store.fallecimiento')}}">
                @csrf
               
                @isset($id)
                <input type="hidden" name="clinical_histories_id" value="{{$id}}">
                @endisset
                @include('partials.form-actas-fallecimientos')
                <button type="submit" class="btn btn-success mt-4">Guarda acta</button>

                </form>
    </div>
</div>

</div>

@endsection
@section('scripts')
<script text="text/javascript">
    var active = 1;
    $(function () {});
</script>
@endsection
