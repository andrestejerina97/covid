@extends('layouts.public')
@section('content')
<header>
    <nav class="navbar navbar-light bg-home text-center">
        <div class="mx-auto">
            <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
            </h2>
            <a class="navbar-brand mb-0 h1 text-right" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
             {{ __('Salir') }}
         </a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
         </form>
        </div>
    </nav>
</header>
<div class="container">

    <div class="row mt-5 ml-1">
        <div class=" mx-auto ">
            <h4 class="text-center">HOJA DE EVOLUCIÓN MÉDICA</h4>
            <hr>
            <form id="form_1" action="{{route('update.ingreso')}}" method="POST"  enctype="multipart/form-data">

            @foreach ($patients as $patient)                
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-5">
                    <div class="form-group">
                        <label for="Fecha">Fecha:</label>
                <input type="date" class="form-control" name="date" value="{{date("Y-m-d")}}" >
                </div>
                <div class="form-group">
                    <label for="Fecha">Nombre:</label>
                <input type="text" name="name"  class="form-control" value="{{$patient->first_name}} {{$patient->second_name}}" >
                </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-2">
                 
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-5">
                    <div class="form-group">
                        <label for="Fecha">Hora:</label>
                    <input type="time" name="time" class="form-control" value="{{date('H:i:s')}}" >
                   </div>
                   <div class="form-group">
                    <label for="Fecha">Apellido:</label>
                    <input type="text" name="lastname" class="form-control" value="{{$patient->first_lastname}} {{$patient->second_lastname}}" >
                </div>
                </div>
            </div>
            <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                <div class="form-group col-md">
                    <label for="">Clasificación TRIAGE:</label>
                </div>
                <div class="form-check" style="background-color: #ff0000">
                    <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
                        value="ff0000"  data-triage="ff0000"
                        @isset($patient->triage)
                        @if ($patient->triage=="ff0000")
                            checked
                    
                        @endif
                        
                        @endisset  >
                    <label class="form-check-label" for="exampleRadios1">
                        Estado Critico o Descompensado
                    </label>
                </div>
                <div class="form-check" style="background-color: #ee8625">
                    <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
                        value="ee8625"
                        @isset($patient->triage)
                        @if ($patient->triage=="ee8625")
                        checked
                        
                     @endif
                        @endisset >
                    <label class="form-check-label" for="exampleRadios2">
                        Emergencia
                    </label>
                </div>
                <div class="form-check" style="background-color: #e7ff0afb">
                    <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
                        value="e7ff0afb"
                        @isset($patient->triage)
                        @if ($patient->triage=="e7ff0afb")
                        checked
                       
                        @endif
                      
                        @endisset >
                    <label class="form-check-label" for="exampleRadios1">
                        Urgencia
                    </label>
                </div>
                <div class="form-check" style="background-color: #1fc23a">
                    <input  onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
                        value="1fc23a"
                        @isset($patient->triage)
                        @if ($patient->triage=="1fc23a")
                        checked
                       
                        @endif
                      
                        @endisset >
                    <label class="form-check-label" for="exampleRadios2">
                        Urgencias Menores
                    </label>
                </div>
                <div class="form-check" style="background-color: #c9b6b6">
                    <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
                        value="c9b6b6"
                        @isset($patient->triage)
                        @if ($patient->triage=="c9b6b6")
                        checked
                       
                        @endif
                  
                        @endisset >
                    <label class="form-check-label" for="exampleRadios2">
                        Fallecido
                    </label>
                </div>
                <div class="form-check" style="background-color: #392ed1">
                    <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
                        value="392ed1" 
                        @isset($patient->triage)
                        @if ($patient->triage=="392ed1")
                        checked
                         
                        @endif    
                
                        @endisset >
                    <label class="form-check-label" for="exampleRadios2">
                        Alta Hospitalaria
                    </label>
                </div>
                
                </div>
                <hr>

                <h4 class="text-center">Datos de test</h4>
                <hr>

            <div class="form-group col-md">
                <label for="">Seleccionar tipo de test:</label>
            </div>
            <div class="form-group">
                <select  class="form-control" name="test_type" id="test_type">
                    <option  value="prueba rapida" selected>Prueba rápida</option>
                    <option  value="pcr/hisopado">PCR/ hisopado</option>
            
                </select>
            </div>
          
            <div class="form-group col-md">
                <label for="">Fecha de realización</label>
                <input type="date" name="date" id="date" class="form-control" 
                @isset($patient->date)
                disabled
            value="{{$patient->date}}"
            @endisset >
            </div>
            <div class="form-group col-md">
                <label for="">Resultado</label>

                <input type="text" tabindex="1" name="result" id="result" class="form-control" placeholder="Escriba el esultado
            "   >
            </div>

            <hr>

            @endforeach

            <!-- section forms-->
                  <input type="hidden" name="clinical_histories_id" value="{{$id}}">
                 

                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-4-form')
                        <br>
                        <textarea placeholder="Observaciones.............." class="form-control" name="observations" id="observation" rows="4"></textarea>

                    </div>
                    <!-- End section forms -->

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit"  class="btn btn-success">Guardar en el historial
                    </button>      
                
            </form>

        </div>

      



    </div>

</div>

@endsection
@section('scripts')
<script text="text/javascript">
    var active = 1;
    $(function () {});
        
    }
</script>
@endsection
