@extends('layouts.public')
@section('content')
<header>
    <nav class="navbar navbar-light bg-home text-center">
        <div class="mx-auto">
            <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
            </h2>
            <a class="navbar-brand mb-0 h1 text-right" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
             {{ __('Salir') }}
         </a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
         </form>
        </div>
    </nav>
</header>
<div class="container">

    <div class="row mt-5 ml-1">
        <div class=" mx-auto">
            <h4>Historial clínico</h4>
            <hr>
            <!-- section forms-->

                <input type="hidden" name="id" value="{{$id}}">
                    @foreach ($patients as $patient)
                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-6-form')

                    </div>
                        <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-1-form')
                    </div>
                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-2-form')

                    </div>
                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-3-form')

                    </div>
                    @isset($login_details)
                    @foreach ($login_details as $login_detail)
                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-4-form')
                    </div>
                    @endforeach
                    @endisset
                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-5-form')

                    </div>

                    <!-- End section forms -->

                    @endforeach

                        <hr>
                    <br>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @isset($details_history_clinicals)
                    @foreach ($details_history_clinicals as $details_history_clinical)
                    <hr>
                    <br>
                    <h4>HOJA DE EVOLUCIÓN CORRESPONDIENTE A :</h4>
                    <h5>Fecha: {{$details_history_clinical->date}}  HORA: {{$details_history_clinical->time}}</h5>
                    <h5>Nombre: {{$details_history_clinical->name}}  Apellido: {{$details_history_clinical->lastname}}</h5>
                    <hr>
                    @php
                     $patient=$details_history_clinical;
                     $detail_symptoms=$detail_symptoms_historial;
                    @endphp
                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        <div class="form-group col-md">
                            <label for="">Clasificación TRIAGE:</label>
                        </div>
                        <div class="form-check" style="background-color: #ff0000">
                            <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="{{$patient->id}}" id="{{$patient->id}}"
                                value="ff0000"  data-triage="ff0000"
                                @if ($patient->triage =="ff0000")
                                    checked
                                    @else
                                    disabled
                                @endif
                                
                                >
                            <label class="form-check-label" for="exampleRadios1">
                                Estado Critico o Descompensado
                            </label>
                        </div>
                        <div class="form-check" style="background-color: #ee8625">
                            <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="{{$patient->id}}" id="{{$patient->id}}"
                                value="ee8625"
                                @if ($patient->triage=="ee8625")
                                checked
                                @else
                                disabled
                            @endif
                                >
                            <label class="form-check-label" for="exampleRadios2">
                                Emergencia
                            </label>
                        </div>
                        <div class="form-check" style="background-color: #e7ff0afb">
                            <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="{{$patient->id}}" id="{{$patient->id}}"
                                value="e7ff0afb"
                                @if ($patient->triage=="e7ff0afb")
                                checked
                                @else
                                disabled
                                @endif
                                >
                            <label class="form-check-label" for="exampleRadios1">
                                Urgencia
                            </label>
                        </div>
                        <div class="form-check" style="background-color: #1fc23a">
                            <input  onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="{{$patient->id}}" id="{{$patient->id}}"
                                value="1fc23a"
                                @if ($patient->triage=='1fc23a')
                                checked
                                @else
                                disabled
                                @endif
                                >
                            <label class="form-check-label" for="exampleRadios2">
                                Urgencias Menores
                            </label>
                        </div>
                        <div class="form-check" style="background-color: #c9b6b6">
                            <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="{{$patient->id}}" id="{{$patient->id}}"
                                value="c9b6b6"
                                @if ($patient->triage=="c9b6b6")
                                checked
                                @else
                                disabled
                                @endif
                                >
                            <label class="form-check-label" for="exampleRadios2">
                                Fallecido
                            </label>
                        </div>
                        <div class="form-check" style="background-color: #392ed1">
                            <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="{{$patient->id}}" id="{{$patient->id}}"
                                value="392ed1" 
                                @if ($patient->triage=="392ed1")
                                checked
                                @else
                                disabled
                                @endif        
                                >
                            <label class="form-check-label" for="exampleRadios2">
                                Alta Hospitalaria
                            </label>
                        </div>
                        
                        
                    </div>


                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-4-form')

                    </div>
                    <!-- End section forms -->
                   
                    <textarea name="observations" id="observations" class="form-control" rows="5">{{$details_history_clinical->observations}}</textarea>

                    @endforeach
                    @endisset
      
        </div>
    </div>

</div>

@endsection
@section('scripts')
<script text="text/javascript">
    var active = 1;
    $(function () {});
        
    }
</script>
@endsection
