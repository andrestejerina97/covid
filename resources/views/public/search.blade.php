<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Covid-19</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <script
			  src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			  crossorigin="anonymous"></script>
    </head>
    <body>

      
        <header>
          <nav class="navbar navbar-light bg-home text-center">
            <div class="mx-auto">
              <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
              </h2>           
              <a class="navbar-brand mb-0 h1 text-right" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
               {{ __('Salir') }}
           </a>

           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
               @csrf
           </form>
            </div>
          </nav>
        </header>
        <div class="container">

            <div class="row  mx-auto mt-5 ">
                <div class="mx-auto">
                    <h2 class="navbar-brand mb-0 h1 text-center">Historias clínicas registradas</h2>
                  </div>
                        <div class="col-lg-4"></div>
                       
                        <div class="table-responsive">
                          <form method="post"  action="{{route('search.patient')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-12 col-lg-12">
                                    <label for=""></label>
                                <div class="input-group col-12">

                                    <input type="text" name="filter" id="filter" tabindex="1" class="form-control" 
                                    placeholder="ingrese número de ID" required>
                                    <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-success ">Buscar</button>
                                </div>
                                </div>
                            </div>
                        </form>
                            <table class="table table-hover">
                                <tr>
                                <th>Paciente</th>
                                    <th>Fecha</th>
                                    <th>Nombre</th>
                                    <th></th>
                                    <th></th>

                                </tr>
                                <tbody>
                                    @isset($clinical_histories)
                                    @foreach ($clinical_histories as $clinic_history)
                                       <tr style="background-color:#{{$clinic_history->triage}};">
                                        <td> {{$clinic_history->id}}. <img style="max-height: 60%; max-width:60%" src="{{asset('img/avatar.png')}}" loading="lazy" alt=""> </td>
                                        <td>{{$clinic_history->created_at}}</td>
                                        <td>{{$clinic_history->first_name}} 
                                          {{$clinic_history->second_name}}, 
                                          {{$clinic_history->first_lastname}} 
                                          {{$clinic_history->second_lastname}} 
                                        </td>
                                      <td><a class="btn btn-success" href="{{route('update.ingreso.get',$clinic_history->id)}}">Ver</a></td>
                                        <td>
                                          @switch($clinic_history->triage)
                                              @case('c9b6b6')
                                              <a class="btn btn-success font-weight-bold" href="{{route('new.clinical_record_dead',$clinic_history->id)}}">+</a>
   
                                                @break
                                              @case(2)
                                                  
                                                  @break
                                              @default
                                              <a class="btn btn-success font-weight-bold" href="{{route('new.clinical_record',$clinic_history->id)}}">+</a>

                                          @endswitch
                                        </td>

                                        </tr>
                                    @endforeach
                                        
                                    @endisset
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination">
                          <li class="page-item"><a class="page-link" href="{{$clinical_histories->previousPageUrl()}}"> atrás
                          </a></li>
                            <li class="page-item"><a class="page-link" href="#">{{$clinical_histories->currentPage()}}</a></li>
    
                          <li class="page-item"><a class="page-link" href="{{$clinical_histories->nextPageUrl()}}">siguiente</a></li>
                        </ul> 
                        <div class="col-lg-4">

                        </div>

            </div>

        </div>
      <!-- <a href="#" class="thumbnail btn">
        <figure>
          <img src="http://i.stack.imgur.com/g1Ce8.png" alt="bg" />
          <figcaption>
            <div>
            Caption here
            </div>
          </figcaption>
        </figure>
      </a> -->
      <script type="text/javascript">


    </script>
    </body>
</html>
