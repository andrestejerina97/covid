@extends('layouts.public')
@section('css')
<link rel="stylesheet" href="{{asset("dist/css/qrcode-reader.css")}}">

@endsection
@section('content')
<header>
    <nav class="navbar navbar-light bg-home text-center">
        <div class="mx-auto">
            <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
            </h2>
            <a class="navbar-brand mb-0 h1 text-right" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
             {{ __('Salir') }}
         </a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
         </form>
        </div>
    </nav>
</header>
<div class="container">

    <div class="row mt-5 ml-1">
        <div class=" mx-auto">
            <h4>Nuevo ingreso</h4>
            <hr>
            <!-- section forms-->
           

            <form method="POST" action="{{route('store.ingreso')}}" class="mt-3" accept-charset="UTF-8"
                enctype="multipart/form-data">
                
                <div class="tab-content" id="nav-tabContent">

                    <div class="tab-pane active" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-1-form')
                        <div class="frow justify-content-center mt-4">
                            <button type="button" onclick="prev_tab(this);" class="btn btn-dark"
                                data-dismiss="modal" id="btn-prev-section-1" disabled>volver</button>
                            <button type="button" onclick="next_tab(this);" class="btn btn-success"
                                data-dismiss="modal" id="btn-next-section-1" data-toggle="tooltip" title="Debe completar ambos campos para continuar!" >Siguiente</button>
                    
                        </div>
                    </div>
                    <div class="tab-pane" id="nav-2" role="tabpanel" aria-labelledby="2-tab">
                        @include('partials.income-2-form')
                        <div class="frow justify-content-center mt-2 mb-2">
                            <button type="button" onclick="prev_tab(this);" class="btn btn-dark"
                                data-dismiss="modal" >volver</button>
                            <button type="button"  onclick="next_tab(this);" class="btn btn-success"
                                data-dismiss="modal">Siguiente</button>
                        </div>
                    
                    </div>
                    <div class="tab-pane" id="nav-3" role="tabpanel" aria-labelledby="3-tab">
                        @include('partials.income-3-form')
                        <div class="frow justify-content-center mt-3">
                            <button type="button" onclick="prev_tab(this);" class="btn btn-dark"
                                data-dismiss="modal">volver</button>
                            <button type="button" onclick="next_tab(this);" class="btn btn-success"
                                data-dismiss="modal">Siguiente</button>

                        </div>
                    </div>
                    <div class="tab-pane" id="nav-4" role="tabpanel" aria-labelledby="4-tab">
                        @include('partials.income-4-form')
                        <div class="frow justify-content-center mt-2 mb-2">
                            <button type="button" onclick="prev_tab(this);" class="btn btn-dark"
                                data-dismiss="modal">volver</button>
                            <button type="button" onclick="next_tab(this);" class="btn btn-success"
                                data-dismiss="modal">Siguiente</button>
                        </div>
                    </div>
                    <div class="tab-pane" id="nav-5" role="tabpanel" aria-labelledby="5-tab">
                        @include('partials.income-6-form')

                        <div class="frow justify-content-center mt-3">
                            <button type="button" onclick="prev_tab(this);" class="btn btn-dark"
                                data-dismiss="modal">volver</button>
                            <button type="button" onclick="next_tab(this);" class="btn btn-success"
                                data-dismiss="modal">Siguiente</button>
                        </div>
                    </div>

                    <!-- End section forms -->
                    <div class="tab-pane" id="nav-6" role="tabpanel" aria-labelledby="6-tab">
                        @include('partials.income-5-form')

                        <div class="frow justify-content-center mt-2 mb-2">
                            <button type="button" onclick="prev_tab(this);" class="btn btn-dark"
                                data-dismiss="modal">volver</button>
                            <button type="submit" class="btn btn-success">Guardar
                            </button>
                        </div>
                    </div>


                    @csrf

            </form>

           
        </div>
<br><br>
<footer class="page-footer font-small blue pt-4"> 

    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab"
                aria-controls="nav-1" aria-selected="true">Institución</a>
            <a class="nav-item nav-link disabled" id="nav-2-tab" data-toggle="tab" href="#nav-2" role="tab"
                aria-controls="nav-2" aria-selected="false">Paciente</a>
            <a class="nav-item nav-link disabled" id="nav-3-tab" data-toggle="tab" href="#nav-3"
                role="tab" aria-controls="nav-3" aria-selected="false">Enfermedades base</a>
            <a class="nav-item nav-link disabled" id="nav-4-tab" data-toggle="tab" href="#nav-4"
                role="tab" aria-controls="nav-4" aria-selected="false">Datos de ingreso</a>
            <a class="nav-item nav-link disabled" id="nav-5-tab" data-toggle="tab" href="#nav-5"
                role="tab" aria-controls="nav-5" aria-selected="false">TRIAGE</a>
            <a class="nav-item nav-link disabled" id="nav-6-tab" data-toggle="tab" href="#nav-6"
                role="tab" aria-controls="nav-6" aria-selected="false">Covid 19</a>
        </div>
    </nav>
</footer>
</div>


    </div>

</div>

@endsection
@section('scripts')

<script src="{{asset("dist/js/qrcode-reader.min.js?v=20190604")}}"></script>
<script text="text/javascript">
    var active = 1;

    $(function(){

// overriding path of JS script and audio 
$.qrCodeReader.jsQRpath = "{{asset('dist/js/jsQR/jsQR.min.js?v=20190604')}}";
$.qrCodeReader.beepPath = "{{asset('dist/audio/beep.mp3')}}";

// bind all elements of a given class
//$("#single2").qrCodeReader();

$("#single2").qrCodeReader({callback: function(code) {
      if (code) {
        $("#2proceedings_number").val(code);
      }  
    }}).off("click.qrCodeReader").on("click", function(){
      var qrcode = $("#single").val().trim();
      if (qrcode) {
        $("#2proceedings_number").val(qrcode);
      } else {
        $.qrCodeReader.instance.open.call(this);
      }
    });
// bind elements by ID with specific options


// read or follow qrcode depending on the content of the target input


});
    function enviar_patient(form,e) {
        e.preventDefault();
      
        $.ajax({
            url: '',
            type: 'post',
            data: $(form).serialize(),
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            dataType: 'json',
            success: function(response) {
                for (let index = 0; index < response.length; index++) {
                    if (aux==1) {
                    $("#"+id_seleccionado).css('display','block');
 
                    }else{
                        alert("guardado con éxito");
                    }
              }

            },
            error:function(res,x,d){
                console.log(res);
            },
       
      });

    }
    var aux=0;
    var id_seleccionado="";
    function change_checked_triage(element) {
       if ($(element).prop("checked") ) {
           let val=$(element).val();
           switch (val) {
               case '392ed1':
                  // $("#section_acta_fallecido").css('display','block');
                        aux=1;
                   //$("#section_acta_alta").css('display','none');
                   id_seleccionado="section_acta_fallecido";

                   break;
                case 'c9b6b6':
              //  $("#section_acta_fallecido").css('display','none');
                //   $("#section_acta_alta").css('display','block');
                   aux=1;
                   id_seleccionado="section_acta_alta";

                break;
                default:
                aux=0;
                $("#section_acta_fallecido").css('display','none');
                   $("#section_acta_alta").css('display','none');
                break;
           }
       }
    };
    function next_tab(element) {
        $(element).parent().parent().removeClass("active");
        let aux = parseInt(active) + 1;
        $("#nav-" + aux + "-tab").addClass("active");
        $("#nav-" + active + "-tab").removeClass("active");

        $("#nav-" + aux).addClass("active");
        active = aux;
    }

    function prev_tab(element) {
        $(element).parent().parent().removeClass("active");
        let aux = parseInt(active) - 1;
        $("#nav-" + aux + "-tab").addClass("active");
        $("#nav-" + active + "-tab").removeClass("active");

        $("#nav-" + aux).addClass("active");
        active = aux;
    }
    function select_departament(element) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let departament=$(element).val();
        $.ajax({
            url: '{{ route("get.intitute") }}',
            type: 'get',
            data: 'departament='+departament,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            dataType: 'json',
            success: function(response) {
                let options='<select class="form-control"  id="institution_name" name="institution_name">'; 
              for (let index = 0; index < response.institutes.length; index++) {
                options+="<option value='"+response.institutes[index].name+"'>"+response.institutes[index].name +"</option>";
   
              }
            
               options+='</select>';
               $("#div_institution").html(options);
               $("#div_institution").css('display','block');

            }   
       
      });
    }
    function select_departament_source(element) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let departament=$(element).val();
        $.ajax({
            url: '{{ route("get.intitute") }}',
            type: 'get',
            data: 'departament='+departament,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false,
            dataType: 'json',
            success: function(response) {
               if (response.institutes.lenght =!0) {
                let options='<select class="form-control "  id="institution_source" name="institution_source">'; 
                for (let index = 0; index < response.institutes.length; index++) {
                options+="<option value='"+response.institutes[index].name+"'>"+response.institutes[index].name +"</option>";
   
              }
               options+='</select>';
               $("#div_institution_source").html(options);
               $("#div_institution_source").css('display','block');
                $("#btn-next-section-1").attr('disabled',false);
               }
            }   
       
      });
    }
    var check_otro=0;
    var check_otro2=0;
    function change_otra_enfermedad(a) {
        if ($(a).data("text")=="Otros") {
            if (check_otro==0) {
                $("#input_disease_other").css("display","block");
                check_otro=1;

            }else{
                $("#input_disease_other").css("display","none");

                check_otro=0;
            }
        }
    }
    function change_otra_alergia(a) {
        if ($(a).data("text")=="OTROS") {
            if (check_otro2==0) {
                $("#input_allergic_other").css("display","block");
                check_otro2=1;

            }else{
                $("#input_allergic_other").css("display","none");

                check_otro2=0;
            }
        }  
    }

    function change_identification_check(element){
        let check=$(element).val();
        if (check=='no') {
            $(".proceedings_number").css('display','none');
        }else{
            $(".proceedings_number").css('display','block');
   
        }
    }
    function change_source_institution_check(element) {
        let check=$(element).val();
        if (check=='no') {
            $("#div_source_institution").css('display','none');
        }else{
            $("#div_source_institution").css('display','block');

        }
    }
</script>
@endsection
