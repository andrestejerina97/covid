<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Covid-19</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/Chart.min.css')}}">

    <script
			  src="https://code.jquery.com/jquery-3.5.1.min.js"
			  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/hammerjs@2.0.8"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.7"></script>
    </head>
    <body>

      
        <header>
          <nav class="navbar navbar-light bg-home text-center">
            <div class="mx-auto">
              <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
              </h2>   
              <a class="navbar-brand mb-0 h1 text-right" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
               {{ __('Salir') }}
           </a>

           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
               @csrf
           </form>
            </div>
          </nav>
        </header>
        <div class="container-fluid">
            <div class=" row mt-4 mx-auto text-center">
                <h1 class="">Resumen</h1>
              </div>
              <div class=" row mt-4 mx-auto">
                <p class=" text-center">Seleccione alguno de los filtros:</p>
              </div>
              <div class="mx-auto">
              <a href="#div_edades" class="btn btn-success" >Edades</a>
              <a href="#div_generos" class="btn btn-success ml-4" >Géneros</a>
              <a href="#div_diagnosticos" class="btn btn-success ml-4" >Triage</a>            
              </div>
            <div class="row   mt-5 ">
             
                <!--  <div class="col-lg-4">
                    <form method="post"  action="{{route('search.patient')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-12 col-lg-12">
                                <label for=""></label>
                            <div class="input-group col-12">
                                <select class="form-control" name="filter" id="filter">
                                    <option value="edad">edad</option>
                                    <option value="genero">genero</option>
                                    <option value="genero">test covid</option>

                                </select>
                                <input type="text" name="search" id="searchMedicines" tabindex="1" class="form-control" 
                                placeholder="" required>
                                <input type="hidden" class="item_token" id="_token" name="_token" value="{{ csrf_token() }}">
          
                                <button type="submit" class="btn btn-success ">Buscar</button>
                            </div>
                            </div>
                        </div>
                    </form>
                  </div> -->
                 

                        <div class="col-lg-12 col-xs-12">
                          
                          <h4 class="text-center">Clasificación por Edades</h4>

                          <div id="div_edades" >
                            <canvas id="edades"></canvas>
                        </div>
                        </div>
                        
 

            </div>
            <div class="row mt-5">
              <div class="col-lg-10">
                <h4 class="text-center">Clasificación por Géneros</h4>
              <div id="div_generos" >
                <canvas id="generos"></canvas>
            </div>
            </div>
          </div>
            <hr class="border-1">
              
            <div class="row mt-5">

              <div class="col-lg-10">
                  <h4 class="text-center">Recuperados-Fallecidos-Contagiados</h4>
                
                <div id="div_diagnosticos" class="chart-container" >
                <canvas id="diagnosticos"></canvas>
              </div>
            </div>
            </div>

        </div>
      <!-- <a href="#" class="thumbnail btn">
        <figure>
          <img src="http://i.stack.imgur.com/g1Ce8.png" alt="bg" />
          <figcaption>
            <div>
            Caption here
            </div>
          </figcaption>
        </figure>
      </a> -->
    <script src="{{asset('js/Chart.min.js')}}"></script>
      <script type="text/javascript">
var edades = document.getElementById('edades').getContext('2d');
var edadesChart = new Chart(edades, {
    type: 'bar',
    data: {
      display: true,
        labels:  [@foreach ($rangeage as  $key => $node)
                  '{{$key}}',
                @endforeach],
        datasets: [{
            label: 'Edades (años)',
            data: [ @foreach ($rangeage as  $key => $node)
                  {{$node}},
                @endforeach],
      
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    max: 100000,
                    min: 0,
                    stepSize: 10000,
                }
            }]
        }
    }
});

var generos = document.getElementById('generos').getContext('2d');
var generosChart = new Chart(generos, {
    type: 'bar',
    data: {
      display: true,

        labels:  [@foreach ($gender as  $key => $node)
                  '{{$key}}'+' '+{{$node}},
                @endforeach],
        datasets: [{
          label: '',

            data: [ @foreach ($gender as  $key => $node)
                  {{$node}},
                @endforeach],
                backgroundColor:[ 'rgb(0, 0, 255)','rgb(255, 8, 202)'],      
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    max: 100000,
                    min: 0,
                    stepSize: 10000,
                }
            }]
        }
    }
});
var diagnosticos = document.getElementById('diagnosticos').getContext('2d');
var diagnosticosChart = new Chart(diagnosticos, {
    type: 'bar',
    data: {
      display: true,
        labels:  [@foreach ($diagnostic as  $key => $node)
                  '{{$key}}'+' '+'{{$node}}',
                @endforeach],
        datasets: [{
            label: 'Resultados generales',
            data: [ @foreach ($diagnostic as  $key => $node)
                  {{$node}},
                @endforeach],
      
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    max: 100000,
                    min: 0,
                    stepSize: 10000,
                }
            }]
        }
    },
 


});



    </script>
    </body>
</html>
