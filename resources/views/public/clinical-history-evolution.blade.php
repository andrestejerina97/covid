@extends('layouts.public')
@section('content')
<header>
    <nav class="navbar navbar-light bg-home text-center">
        <div class="mx-auto">
            <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
            </h2>
            <a class="navbar-brand mb-0 h1 text-right" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
             {{ __('Salir') }}
         </a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
         </form>
        </div>
    </nav>
</header>
<div class="container">

    <div class="row mt-5 ml-1">
        <div class=" mx-auto">
            <h4>HOJA DE EVOLUCIÓN MÉDICA</h4>
            <hr>
            <!-- section forms-->
            <form id="form_1" action="{{route('update.ingreso')}}" method="POST"
                enctype="multipart/form-data">
                    @foreach ($details_history_clinicals as $details_history_clinical)
                    <div class="form-row">
                    <label for=""> Fecha de creación: {{$details_history_clinical->created_at}}</label>
                    </div>
                    @php
                     $patient=$details_history_clinical;
                     $detail_symptoms=$detail_symptoms_historial;
                    @endphp
                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-4-form')

                    </div>
                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-5-form')

                    </div>

                    <!-- End section forms -->
                    <div class="card-body" id="nav-1" role="tabpanel" aria-labelledby="1-tab">
                        @include('partials.income-6-form')

                    </div>
              
                    @endforeach
                  
                    @isset($patient->triage)
                    @switch($patient->triage)
                        @case('ff0000')
                        <button type="submit" class="btn btn-success">Modificar
                        </button>  
                            @break
                        @case('ee8625')
                        <button  type="submit"  class="btn btn-success">Modificar
                        </button>  
                            @break
                        @case('e7ff0afb')
                        <button type="submit" class="btn btn-success">Modificar
                        </button>  
                        @break
                        @case('1fc23a')
                        <button type="submit" class="btn btn-success">Modificar
                        </button>  
                        @break
                        @case('c9b6b6')
                        <button  type="submit" class="btn btn-success">Modificar
                        </button>  
                        @break
                        @case('392ed1')
                        
                        @break
                        @default
                        
                    @endswitch
                    @endisset
                
                    <button type="submit" class="btn btn-success">Guardar
                    </button>  

        </div>
    </div>

</div>

@endsection
@section('scripts')
<script text="text/javascript">
    var active = 1;
    $(function () {});
        
    }
</script>
@endsection
