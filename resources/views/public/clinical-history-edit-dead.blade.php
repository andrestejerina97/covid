@extends('layouts.public')
@section('content')
<header>
    <nav class="navbar navbar-light bg-home text-center">
        <div class="mx-auto">
            <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
            </h2>
            <a class="navbar-brand mb-0 h1 text-right" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
             {{ __('Salir') }}
         </a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
         </form>
        </div>
    </nav>
</header>
<div class="container">

    <div class="row mt-5 ml-1">
        <div class=" mx-auto ">
            <h4 class="text-center">Datos de test</h4>
            <hr>
            <form id="form_1" action="{{route('update.ingreso')}}" method="POST"  enctype="multipart/form-data">

         
            <div class="form-group col-md">
                <label for="">Seleccionar tipo de test:</label>
            </div>
            <div class="form-group">
                <select  class="form-control" name="test_type" id="test_type">
            
            
                    <option  value="prueba rapida" selected>Prueba rápida</option>
                    <option disabled value="pcr/hisopado">PCR/ hisopado</option>
            
                </select>
            </div>
          
            <div class="form-group col-md">
                <label for="">Fecha de realización</label>
                <input type="date" name="date" id="date" class="form-control" 
                @isset($patient->date)
                disabled
            value="{{$patient->date}}"
            @endisset >
            </div>
            <div class="form-group col-md">
                <input type="text" tabindex="1" name="result" id="result" class="form-control" placeholder="Resultado
            "  @isset($patient->result)
            disabled
            value="{{$patient->result}}"
            @endisset >
            </div>

            <hr>

            
            <!-- section forms-->
                  <input type="hidden" name="clinical_histories_id" value="{{$id}}">
                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit"  class="btn btn-success">Guardar en el historial
                    </button>      
                
            </form>

        </div>

      



    </div>

</div>

@endsection
@section('scripts')
<script text="text/javascript">
    var active = 1;
    $(function () {});
        
    }
</script>
@endsection
