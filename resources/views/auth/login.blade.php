@extends('layouts.public')

@section('content')
 <div id="background" class="preloader">
 </div>
    <div id="logocontainer" class="preloader" >
        <div class="row">
            <div class="col-lg-3 col-md-3 col-2"></div>
            <div class="col-lg-6 col-md-6 col-8">
                <div class="text-center">
                    <button onclick=fill() class="btn btn-primary">Iniciar sesión</button>
                </div>
                <br>
              <img  style="max-height: 100%; max-width:100%" src="{{asset('img/bandera.png')}}" alt="">
            </div>
            <div class="col-lg-3 col-md-3 col-2"></div>

        </div>
      <div id="pelogo">
         

      </div>
      
    </div>
        <header>
          <nav class="navbar navbar-light bg-home text-center">
            <div class="mx-auto">
              <h2 class="navbar-brand mb-0 h1 text-center"><a class="text-light" href="{{url('/')}}"> COVID-19</a>
              </h2>            </div>
          </nav>
        </header>
        <br> <br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Bienvenido, introduce tus credenciales para acceder al sistema:') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                   <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a> -->
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    var full=false;

function fill(){
 $('#logocontainer').delay(200).fadeOut('slow');
 setTimeout(function() {
       // After 2s, the no-scroll class of the body will be removed
       $('#background').removeClass('preloader');
       $('#background').attr('id','2');

    }, 10); //Here you can change preloader time
  
}
  //After 2s preloader is fadeOut
 // $('#logocontainer').delay(2000).fadeOut('slow');
    setTimeout(function() {
        //After 2s, the no-scroll class of the body will be removed
    //    $('#background').removeClass('preloader');
      //  $('#background').attr('id','2');

    }, 2000); //Here you can change preloader time
    </script>
@endsection
