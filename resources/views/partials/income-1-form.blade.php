    
             @isset($patient->institution_source_id)
             <div class="form-group col-md">

             <label for="">Institución:</label>
             <input type="text" class="form-control" value="{{$patient->name}}" disabled>
             </div>
             <div class="form-group col-md">
                <label for="">Institución de procedencia:</label>
                <input type="text" class="form-control" value="{{$patient->institution_source_id}}" disabled>

            </div>
             @else
             <div class="form-row">
                <div class="form-group col-md">
                    <label for="">Nombre de departamento:</label>
                    @isset($patient->institution_source_id)
                    <input type="text" class="form-control" value="{{$patient->institution_source_id}}" disabled>
                    @else
                    <select onchange="select_departament(this);" name="departament" id="departament"
                    class="form-control">
                    <option value="0" selected>Seleccionar</option>
                    <option value="Francisco Morazan">Francisco Morazán</option>
                    <option value="Cortes">Cortes</option>
                    </select>
                    @endisset 
                </div>
                <div class="form-group col-md">
                    <label for="">Institución:</label>
                    <div id="div_institution">
                        <select name="departament" id="departament"
                        class="form-control">
                        @isset($patient->institution_id)
                        <option disabled  value="{{$patient->name}}" selected>{{$patient->name}}</option>
                        @else
                        <option value="sin institucion" selected>Sin institución</option>
                        @endisset 
                    </select>
                    </div>
                </div>
            </div>

                
        <div class="form-group col-md">
            <label for="">¿Procede de otra institución?:</label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" onclick="change_source_institution_check(this);" name="check_inst" id="check_inst"
                value="si"  
                @isset($patient->institution_source_id)
                 disabled
                @if($patient->institution_source_id!='sin institucion')
                checked
                @endif
                @else
                @endisset >
            <label class="form-check-label" for="">
                Si
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" onclick="change_source_institution_check(this);" name="check_inst" id="check_inst"
                value="no"
                @isset($patient->institution_source_id)
                disabled
                @if($patient->institution_source_id =='sin institucion')
                checked
                @endif
                @else
                checked
                @endisset >
            <label class="form-check-label" for="">
                No
            </label>
        </div>
        <div id="div_source_institution" style="display: none" class="form-row">
            <div class="form-group col-md">
                <label for="">Nombre de departamento:</label>
                <select onchange="select_departament_source(this);" name="departament" id="departament"
                    class="form-control">
                    @isset($patient->institution_source_id)
                    <option value="Francisco Morazan">Francisco Morazan</option>
                    @else
                    <option value="0" selected>Seleccionar</option>
                    <option value="Francisco Morazan">Francisco Morazan</option>
                    <option value="Cortes">Cortes</option>
                    @endisset 
                    
                </select>
            </div>
        <div  class="form-row institution_source">
            <div class="form-group col-md">
                <label for="">Institución de procedencia:</label>
               <div id="div_institution_source">
                <select name="institution_source" id="institution_source" class="form-control">
                    @isset($patient->institution_source_id)
                    @if($patient->institution_source_id !='sin institucion')
                    <option value="{{$patient->institution_source_id}}" selected>{{$patient->institution_source_id}}</option>
                    @endif
                    @else
                    <option value="sin institucion" selected>Sin institución</option>

                    @endisset 
                </select>
               </div>
            </div>
        </div>

    </div>
   
             @endisset


