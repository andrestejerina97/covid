<div class="form-group col-md">
    <label for="">Seleccionar tipo de test:</label>
</div>
<div class="form-group">
    <select  class="form-control" name="test_type" id="test_type">

        @isset($patient->test_type)
        @if($patient->test_type=='prueba rapida')

        <option  value="prueba rapida" selected>Prueba rápida</option>
        <option disabled value="pcr/hisopado">PCR/ hisopado</option>
        @else
        <option disabled value="prueba rapida" >Prueba rápida</option>
        <option disabled value="pcr/hisopado" selected>PCR/ hisopado</option>
        @endif
        @else
        <option value="prueba rapida" selected>Prueba rápida</option>
        <option value="pcr/hisopado">PCR/ hisopado</option>
        @endisset
  

    </select>
</div>
<div class="form-group col-md">
    <input type="text" tabindex="1" name="place" id="place" class="form-control" placeholder="Lugar de Test
" 
@isset($patient->place)
disabled
value="{{$patient->place}}"
@endisset >
</div>

<div class="form-group col-md">
    <label for="">Fecha de realización</label>
    <input type="date" name="date" id="date" class="form-control" 
    @isset($patient->date)
    disabled
value="{{$patient->date}}"
@endisset >
</div>
<div class="form-group col-md">
    <input type="text" tabindex="1" name="result" id="result" class="form-control" placeholder="Resultado
"  @isset($patient->result)
disabled
value="{{$patient->result}}"
@endisset >
</div>

<div class="form-group col-md">
    <label for="">Fecha de entrega de test</label>
    <input type="date" tabindex="1" name="delivery_date" id="delivery_date" class="form-control"
    @isset($patient->delivery_date)
    disabled
    value="{{$patient->delivery_date}}"
    @endisset >
</div>
<hr>
<h5>Resultados:</h5>
<div class="form-group col-md">
    <input type="text" tabindex="1" name="diagnosis" id="diagnosis" class="form-control"
        placeholder="Diagnostico" 
        @isset($patient->diagnosis)
        disabled
        value="{{$patient->diagnosis}}"
        @endisset >
        
</div>
<div class="form-group col-md">
    <input type="text" tabindex="1" name="cause" id="cause" class="form-control"
        placeholder="Causa"
        @isset($patient->cause)
        disabled
        value="{{$patient->cause}}"
        @endisset >
</div>
<div class="form-group col-md">
    <input type="text" tabindex="1" name="treatment" id="treatment" class="form-control"
        placeholder="Tratamiento" 
        @isset($patient->treatment)
        disabled
        value="{{$patient->treatment}}"
        @endisset >
</div>
<div class="form-group col-md">
    <input type="text" name="process" id="process" class="form-control"
        placeholder="Procedimiento Desarrollado" 
        @isset($patient->process)
        disabled
        value="{{$patient->process}}"
        @endisset >
</div>
<div class="form-group col-md">
    <input type="text" name="responsible_doctor" id="responsible_doctor" class="form-control"
        placeholder="Doctor Responsable"
        @isset($patient->responsible_doctor)
        disabled
        value="{{$patient->responsible_doctor}}"
        @endisset >
</div>
