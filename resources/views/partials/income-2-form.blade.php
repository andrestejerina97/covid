    <!--info paciente -->
     @isset($patient->proceedings_number)
     @else
     <div class="form-group col-md">
        <label for="">¿Tiene identificación?:</label>
    </div>
    <div class="form-check">
        @isset($patients->proceedings_number)
        disabled
        @if ($patient->proceedings_number !="")
        <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="check_ident" id="check_ident"
        value="si" disabled checked >
    <label class="form-check-label" for="">
        Si
    </label>
         
        @endif
        @else  
        <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="check_ident" id="check_ident"
        value="si"  >
        <label class="form-check-label" for="">
            Si
        </label>
        @endisset
   
    </div>
    <div class="form-check">
        @isset($patients->proceedings_number)
        disabled
        @if ($patient->proceedings_number !="")
        <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="check_ident" id="check_ident"
        value="no" disabled checked>
    <label class="form-check-label" for="">
        No
    </label>
        @endif
        @else  
        <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="check_ident" id="check_ident"
        value="no" checked  >
        <label class="form-check-label" for="">
            No
        </label>
        @endisset
        
    </div>
     @endisset
    <hr>
    <div class="form-group col-md">
        @isset($patient->proceedings_number)
        <div  class="proceedings_number">
            <label for="">N° de identificación</label>
        @else
        <div  style="display: none" class="proceedings_number">
    
        @endisset
    
        <input type="text" tabindex="1" name="proceedings_number" id="2proceedings_number"
            class="form-control" placeholder="N° Identificacion(opcional)"
            title="N° Identificacion" 
            @isset($patient->proceedings_number)
            disabled
            value="{{$patient->proceedings_number}}"
            @else
            >
            <input hidden id="single" type="text" size="50"> 

            <br>
            @endisset
    </div>
</div>
    <hr>
    <div class="form-row">
        <div class="form-group col-md">
            <input type="text" tabindex="1" name="first_name" id="first_name" class="form-control"
                placeholder="Primer Nombre" title="Primer Nombre"
                @isset($patient->first_name)
                disabled
                value="{{$patient->first_name}}"
                @endisset >
                
        </div>

        <div class="form-group col-md">
            <input type="text" tabindex="1" name="second_name" id="second_name" class="form-control"
                placeholder="Segundo Nombre" title="Segundo Nombre"
                @isset($patient->second_name)
                disabled
                value="{{$patient->second_name}}"
                @endisset >

        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md">
            <input type="text" tabindex="1" name="first_lastname" id="first_lastname"
                class="form-control" placeholder="Primero Apellido"
                @isset($patient->first_lastname)
                disabled
                value="{{$patient->first_lastname}}"
                @endisset >
        </div>

        <div class="form-group col-md">
            <input type="text" tabindex="1" name="second_lastname" id="second_lastname"
                class="form-control" placeholder="Segundo Apellido"
                @isset($patient->second_lastname)
                disabled
                value="{{$patient->second_lastname}}"
                @endisset >

        </div>
    </div>
    <div class="form-row">


        <div class="form-group col-md">
            <input type="text" tabindex="1" name="occupation" id="occupation" class="form-control"
                placeholder="Ocupación"
                @isset($patient->occupation)
                disabled
                value="{{$patient->occupation}}"
                @endisset >

        </div>
        <div class="form-group">
            <div class="form-group col-md">
                <input type="text" name="blood_type" id="blood_type" tabindex="1"
                    class="form-control" placeholder="Grupo Sanguíneo"
                    @isset($patient->blood_type)
                    disabled
                    value="{{$patient->blood_type}}"
                    @endisset >
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="form-group col-md">
            <label for="">Genero</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="gender"
                    value="femenino" 
                    @isset($patient->gender)
                    disabled
                    @if ($patient->gender == 'femenino')
                        checked
                    @endif
                    @else
                    @endisset >
                <label class="form-check-label" for="">
                    Femenino
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="gender"
                    value="masculino"
                    @isset($patient->gender)
                    disabled
                    @if ($patient->gender == 'masculino')
                        checked
                    @endif
                    @endisset >
                <label class="form-check-label" for="">
                    Masculino
                </label>
            </div>

        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md">
            <label for="">Fecha de Nacimiento</label>
            <input type="date" tabindex="1" name="birhdate" id="birhdate" class="form-control"
                placeholder="Fecha de Nacimiento" 
                @isset($patient->birhdate)
                disabled
                value="{{$patient->birhdate}}"
                @endisset >
        </div>
        <div class="form-group col-md">
            <label for="">Edad</label>

            <input type="text" name="age" id="age" tabindex="2" class="form-control"
                placeholder="Edad"
                @isset($patient->age)
                disabled
                value="{{$patient->age}}"
                @endisset >
                
        </div>

    </div>
   
    <div class="form-row">
        <div class="form-group col-md">
            <input type="text" name="cellphone" id="cellphone" tabindex="1" class="form-control"
                placeholder="Telefono"
                @isset($patient->cellphone)
                disabled
                value="{{$patient->cellphone}}"
                @endisset >
        </div>


    </div>
    <div class="form-row">
        <div class="form-group col-md">
            <input type="text" name="place_birth" id="place_birth" tabindex="1" class="form-control"
                placeholder="Lugar de Nacimiento"
                @isset($patient->place_birth)
                disabled
                value="{{$patient->place_birth}}"
                @endisset >
                
        </div>

        <div class="form-group col-md">
            <input type="text" name="person_charge"  id=" person_charge" class="form-control"
                placeholder="Persona Responsable"
                @isset($patient->person_charge)
                disabled
                value="{{$patient->person_charge}}"
                @endisset >

        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md">
            <input type="text" name="home" tabindex="19" id="home" class="form-control"
                placeholder="Domicilio"
                @isset($patient->home)
                disabled
                value="{{$patient->home}}"
                @endisset >

        </div>
    </div>

    <!--fin info paciente-->
