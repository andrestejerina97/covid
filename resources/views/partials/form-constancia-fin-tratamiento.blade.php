<!--FORM CONSTANCIA FIN DE TRATAMIENTO -->


    <!--info paciente -->
    <div class="form-row">

        <div class="form-group col-md">
            <label for="">Fecha fin de tratamiento</label>
            <input type="date" tabindex="20" name="constancia_fecha_fin_tratamiento" id="constancia_fecha_fin_tratamiento" class="form-control"
                @isset($patient->constancia_fecha_fin_tratamiento)
                value="{{$patient->constancia_fecha_fin_tratamiento}}"
                @endisset >

        </div>
        <div class="form-group col-md">
            <label for="">Hora fin de tratamiento</label>
            <input type="time" tabindex="1" name="constancia_hora_fin_tratamiento" id="constancia_hora_fin_tratamiento" class="form-control"
                @isset($patient->constancia_hora_fin_tratamiento)
                value="{{$patient->constancia_hora_fin_tratamiento}}"
                @endisset >
        </div>
    </div>
    <div class="form-row">
        <label for="">Secuelas incapacitantes</label>
    </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="secuela_incapacitante_fin" id="secuela_incapacitante_fin"
                value="si"
                @isset($patients)
                @if ($patient->tratamiento_pendiente =='Si')
                checked
                @else    
                @endif
                @endisset >
            <label class="form-check-label" for="">
                Si
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="secuela_incapacitante_fin" id="secuela_incapacitante_fin"
                value="no"  
                @isset($patients)
                @if ($patient->tratamiento_pendiente =='No')
                checked
                @else    
                @endif
                @endisset >
            <label class="form-check-label" for="">
                No
            </label>
        </div>
        <!--Recalificación profesional -->
        <div class="form-row">
            <label for="">Recalificación profesional</label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio"  name="recalificacion_profesional_constancia" id="recalificacion_profesional_constancia"
              value="si"
              @isset($patients)
              @if ($patient->recalificacion_profesional_constancia =='Si')
              checked
              @else    
              @endif
              @endisset >
          <label class="form-check-label" for="">
              Si
          </label>
      </div>
      <div class="form-check">
          <input class="form-check-input" type="radio" name="
          <input class="form-check-input" type="radio"  name="recalificacion_profesional_constancia" id="recalificacion_profesional_constancia"
              value="no"  
              @isset($patients)
              @if ($patient->recalificacion_profesional_constancia =='No')
              checked
              @else    
              @endif
              @endisset >
          <label class="form-check-label" for="">
              No
          </label>
      </div>
        <!--fin -->
  <!--prestacioon de manteniemiento-->
  <div class="form-row">
      <label for="">Prestación de manteniemiento</label>
  </div>
  <div class="form-check">
    <input class="form-check-input" type="radio"  name="prestacion_mantenimiento_fin" id="prestacion_mantenimiento_fin"
        value="si"
        @isset($patients)
        @if ($patient->prestacion_mantenimiento_fin =='Si')
        checked
        @else    
        @endif
        @endisset >
    <label class="form-check-label" for="">
        Si
    </label>
</div>
<div class="form-check">
    <input class="form-check-input" type="radio" name="prestacion_mantenimiento_fin" id="prestacion_mantenimiento_fin"
        value="no"  
        @isset($patients)
        @if ($patient->prestacion_mantenimiento_fin =='No')
        checked
        @else    
        @endif
        @endisset >
    <label class="form-check-label" for="">
        No
    </label>
</div>