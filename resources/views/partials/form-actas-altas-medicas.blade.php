  
  <hr>
  <!-- Form Alta médica -->
  <h4>DATOS DEL PACIENTE</h4>
  <div class="form-group col-md">
    <label for="">Apellido y nombre:</label>
</div>
<div class="form-group ">
    <input type="text" tabindex="1" name="nombre_trabajador" id="nombre_trabajador"
        class="form-control"
        @isset($patient->nombre_trabajador)
        value="{{$patient->nombre_trabajador}}"
        @endisset>
</div>
    <div class="form-group ">
        <div class="input-group">
            <div class="form-group ">
                <label for="">Calle</label>
                <input type="text" tabindex="1" name="calle_trabajador" id="calle_trabajador"
                        class="form-control"
                        @isset($patient->calle_trabajador)
                        value="{{$patient->calle_trabajador}}"
                        @endisset>
            </div>
            <div class="form-group ">
                <label for="">Numero</label>
                <input type="text" tabindex="1" name="numero_trabajador" id="calle_trabajador"
                    class="form-control"
                    @isset($patient->numero_trabajador)
                    value="{{$patient->numero_trabajador}}"
                    @endisset>
            </div>
        </div>
        
    </div>
    <div class="form-row ">
        <div class="input-group">
            
   
        <div class="form-group ">
            <label for="">Piso</label>
            <input type="text" tabindex="1" name="piso_trabajador" id="piso_trabajador"
                class="form-control"
                @isset($patient->piso_trabajador)
                value="{{$patient->piso_trabajador}}"
                @endisset>
        </div>
        <div class="form-group ">
            <label for="">Dpto</label>
            <input type="text" tabindex="1" name="departamento_trabajador" id="departamento_trabajador"
                class="form-control"
                @isset($patient->departamento_trabajador)
                value="{{$patient->departamento_trabajador}}"
                @endisset>
        </div>
        <div class="form-group ">
            <label for="">Localidad</label>
            <input type="text" tabindex="1" name="localidad_trabajador" id="localidad_trabajador"
                class="form-control"
                @isset($patient->localidad_trabajador)
                value="{{$patient->localidad_trabajador}}"
                @endisset>
        </div>
    </div>
</div>
<div class="form-row ">
    <div class="input-group">
    <div class="form-group ">
        <label for="">Provincia</label>
        <input type="text" tabindex="1" name="provincia_trabajador" id="provincia_trabajador"
            class="form-control"
            @isset($patient->provincia_trabajador)
            value="{{$patient->provincia_trabajador}}"
            @endisset>
    </div>
    <div class="form-group ">
        <label for="">CP</label>
        <input type="text" tabindex="1" name="cp_trabajador" id="cp_trabajador"
            class="form-control"
            @isset($patient->cp_trabajador)
            value="{{$patient->cp_trabajador}}"
            @endisset>
    </div>
    <div class="form-group ">
        <label for="">Tel.Fijo</label>
        <input type="text" tabindex="1" name="tel_trabajador" id="tel_trabajador"
            class="form-control"
            @isset($patient->tel_trabajador)
            value="{{$patient->tel_trabajador}}"
            @endisset>
    </div>
    <div class="form-group ">
        <label for="">Tel.movil</label>
        <input type="text" tabindex="1" name="calle_trabajador" id="calle_trabajador"
            class="form-control"
            @isset($patient->tel_movil_trabajador)
            value="{{$patient->tel_movil_trabajador}}"
            @endisset>
    </div>
</div>
</div>
<hr>
<h4>DATOS DEL PRESTADOR</h4>

<div class="form-group col-md">
    <label for="">Nombre del establecimiento asistencial:</label>
</div>
<div class="form-group ">
    <input type="text" tabindex="10" name="establecimiento_asistencial" id="establecimiento_asistencial"
        class="form-control"
        @isset($patient->establecimiento_asistencial)
        value="{{$patient->establecimiento_asistencial}}"
        @endisset>
</div>

<hr>
<div class="form-group col-md">
    <label for="">Apellido y nombre:</label>
</div>
<div class="form-group ">
    <input type="text" tabindex="11" name="nombre_completo_prestador" id="nombre_completo_prestador"
        class="form-control"
        @isset($patient->nombre_completo_prestador)
        value="{{$patient->nombre_completo_prestador}}"
        @endisset>
</div>
  <hr>
  <h4>PRUEBA COVID</h4>
  <div class="form-row">
    <div class="form-group col-md">
        <label for="">Fecha de prueba:</label>
        <input type="date" tabindex="12" name="fecha_prueba_covid" id="fecha_prueba_covid" class="form-control"
            placeholder="" 
            @isset($patient->fecha_prueba_covid)
            value="{{$patient->fecha_prueba_covid}}"
            @endisset >
            
    </div>

    <div class="form-group col-md">
        <label for="">Hora:</label>
        <input type="time" tabindex="13" name="hora_prueba_covid" id="hora_prueba_covid" class="form-control"
           
            @isset($patient->hora_prueba_covid)
            value="{{$patient->hora_prueba_covid}}"
            
            @endisset >

    </div>
</div>
<div class="form-row">
    <div class="form-group col-md">
        <label for="">Fecha de resultado:</label>
        <input type="date" tabindex="14" name="fecha_resultado_covid" id="fecha_resultado_covid" class="form-control"
            placeholder="Primer Nombre" title="Fee"
            @isset($patient->fecha_resultado_covid)
            value="{{$patient->fecha_resultado_covid}}"
            @endisset required>
            
    </div>

    <div class="form-group col-md">
        <label for="">Hora:</label>
        <input type="time" tabindex="15" name="hora_resultado_covid" id="hora_resultado_covid" class="form-control"
            placeholder="Segundo Nombre" title="Segundo Nombre"
            @isset($patient->hora_resultado_covida)
            value="{{$patient->hora_resultado_covid}}"
            @endisset required>

    </div>
</div>
<div style="display: none"  class="form-row">
    <div class="form-group col-md">
        <label for="">Fecha de primera atención médica:</label>
        <input  type="date" tabindex="16" name="fecha_primera_atencion" id="fecha_primera_atencion" class="form-control"
            @isset($patient->fecha_primera_atencion)
            value="{{$patient->fecha_primera_atencion}}"
            @endisset >
            
    </div>

    <div  class="form-group col-md">
        <label  for="">Hora:</label>
        <input type="time" tabindex="17" name="hora_primera_atencion" id="hora_primera_atencion" class="form-control"
            @isset($patient->hora_primera_atencion)
            value="{{$patient->hora_primera_atencion}}"
            @endisset >

    </div>
</div>
<div class="form-row">
    <div class="form-group col-md">
        <label for="">Descripción del motivo de consulta:</label>
        <textarea name="descripcion_consulta_motivo" id="descripcion_consulta_motivo" class="form-control">
            @isset($patient->descripcion_consulta_motivo)
           {{$patient->descripcion_consulta_motivo}}
            @endisset
        </textarea>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md">
        <label for="">Diagnóstico:</label>
        <textarea name="diagnostico" id="diagnostico" class="form-control">
            @isset($patient->diagnostico)
           {{$patient->diagnostico}}
            @endisset
        </textarea>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md">
        <label for="">Indicaciones/ tratamiento:</label>
        <textarea name="indicaciones" id="indicaciones" class="form-control">
            @isset($patient->indicaciones)
           {{$patient->indicaciones}}
            @endisset
        </textarea>
    </div>
</div>

  <hr>
  <div class="form-group col-md">
        <label for="">¿Tratamiento médico asistencial pendiente?:</label>
    </div>
    <div class="form-check">
       
        <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="tratamiento_pendiente" id="tratamiento_pendiente"
            value="si"
            @isset($patients)
            @if ($patient->tratamiento_pendiente =='Si')
            checked
            @else    
            @endif
            @endisset >
        <label class="form-check-label" for="">
            Si
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="tratamiento_pendiente" id="tratamiento_pendiente"
            value="no"  
            @isset($patients)
            @if ($patient->tratamiento_pendiente =='No')
            checked
            @else    
            @endif
            @endisset >
        <label class="form-check-label" for="">
            No
        </label>
    </div>
    <hr>
    <div class="form-group ">
        <input type="text" tabindex="1" name="nombre_tratamiento" id="nombre_tratamiento"
            class="form-control" placeholder="Nombre del tratamiento"
            style="display: none"
            @isset($patient->nombre_tratamiento)
            value="{{$patient->nombre_tratamiento}}"
            @endisset>
    </div>
    <hr>
    <div class="form-row">
        <div class="form-group col-md">
            <label for="">Fecha próxima revisión</label>
            <input type="date" tabindex="1" name="fecha_proxima_revision" id="fecha_proxima_revision" class="form-control"
                placeholder="Primer Nombre" title="Fee"
                @isset($patient->fecha_proxima_revision)
                value="{{$patient->fecha_proxima_revision}}"
                @endisset required>
                
        </div>

        <div class="form-group col-md">
            <label for="">Hora de revisión</label>
            <input type="time" tabindex="1" name="hora_proxima_revision" id="hora_proxima_revision" class="form-control"
                placeholder="Segundo Nombre" title="Segundo Nombre"
                @isset($patient->hora_proxima_revision)
                value="{{$patient->hora_proxima_revision}}"
                @endisset required>

        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md">
            <label for="">¿Recalificación profesional?:</label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="recalificacion_profesional" id="recalificacion_profesional"
                value="si" 
                @isset($patients)
                @if ($patient->recalificacion_profesional=='si')
                checked
                @endif               
                @endisset>
            <label class="form-check-label" for="">
                Si
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="recalificacion_profesional" id="recalificacion_profesional"
                value="no" 
                @isset($patients)
                @if ($patient->recalificacion_profesional=='no')
                checked
                @endif
                @else   
                checked
                @endisset
                >
            <label class="form-check-label" for="">
                No
            </label>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md">
            <label for="">Fecha de retorno al trabajo</label>
            <input type="date" tabindex="1" name="fecha_retorno_trabajo" id="fecha_retorno_trabajo"
                class="form-control" 
                @isset($patient->fecha_retorno_trabajo)
                value="{{$patient->fecha_retorno_trabajo}}"
                @endisset required>
        </div>

        <div class="form-group col-md">
            <label for="">Hora de retorno al trabajo</label>
            <input type="time" tabindex="1" name="hora_retorno_trabajo" id="hora_retorno_trabajo" class="form-control"
                @isset($patient->hora_retorno_trabajo)
                value="{{$patient->hora_retorno_trabajo}}"
                @endisset required>
        </div>
    </div>
    <div class="form-row">

        <div class="form-group col-md">
            <label for="">Fecha fin de tratamiento</label>
            <input type="date" tabindex="1" name="fecha_fin_tratamiento" id="fecha_fin_tratamiento" class="form-control"
                @isset($patient->fecha_fin_tratamiento)
                value="{{$patient->fecha_fin_tratamiento}}"
                @endisset required>

        </div>
        <div class="form-group col-md">
            <label for="">Hora fin de tratamiento</label>
            <input type="time" tabindex="1" name="hora_fin_tratamiento" id="hora_fin_tratamiento" class="form-control"
                @isset($patient->hora_fin_tratamiento)
                value="{{$patient->hora_fin_tratamiento}}"
                @endisset required>
        </div>
    </div>
    <div class="form-row">
        <label for="">Motivo de cese de ILT</label>

    </div>
    <div class="form-row">
        <div class="form-check">
            <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="motivo_de_cese" id="motivo_de_cese"
            value="alta medica" 
            @isset($patients)
            @if ($patient->motivo_de_cese=='alta medica')
            checked
            @endif
            @else   
            checked
            @endisset
            >
        <label class="form-check-label" for="">
            alta medica
        </label>
        </div>
       
            <div class="form-check">
                <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="motivo_de_cese" id="motivo_de_cese2"
                    value="Rechazo" 
                    @isset($patients)
                    @if ($patient->recalificacion_profesional=='Rechazo')
                    checked
                    @endif
                    @else   
                    
                    @endisset
                    >
                <label class="form-check-label" for="">
                    Rechazo
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="motivo_de_cese" id="motivo_de_cese2"
                    value="Muerte" 
                    @isset($patients)
                    @if ($patient->recalificacion_profesional=='Muerte')
                    checked
                    @endif
                    @else
                    @endisset
                    >
                <label class="form-check-label" for="">
                    Muerte
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="motivo_de_cese" id="motivo_de_cese2"
                    value="Fin de tratamiento" 
                    @isset($patients)
                    @if ($patient->recalificacion_profesional=='Muerte')
                    checked
                    @endif
                    @else
                    @endisset
                    >
                <label class="form-check-label" for="">
                    Fin de tratamiento
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="motivo_de_cese" id="motivo_de_cese2"
                    value="Por derivación" 
                    @isset($patients)
                    @if ($patient->recalificacion_profesional=='Muerte')
                    checked
                    @endif
                    @else
                    @endisset
                    >
                <label class="form-check-label" for="">
                    Por derivación
                </label>
            </div>
        </div>
        <div class="form-row">
            <label for="">Secuelas incapacitantes</label>
        </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="secuela_incapacitante" id="secuela_incapacitante"
                    value="si"
                    @isset($patients)
                    @if ($patient->tratamiento_pendiente =='Si')
                    checked
                    @else    
                    @endif
                    @endisset >
                <label class="form-check-label" for="">
                    Si
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="secuela_incapacitante" id="secuela_incapacitante"
                    value="no"  
                    @isset($patients)
                    @if ($patient->tratamiento_pendiente =='No')
                    checked
                    @else    
                    @endif
                    @endisset >
                <label class="form-check-label" for="">
                    No
                </label>
            </div>
      <!--prestacioon de manteniemiento-->
      <div class="form-row">
          <label for="">Prestación de manteniemiento</label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio"  name="prestacion_mantenimiento" id="prestacion_mantenimiento"
            value="si"
            @isset($patients)
            @if ($patient->prestacion_mantenimiento =='Si')
            checked
            @else    
            @endif
            @endisset >
        <label class="form-check-label" for="">
            Si
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="prestacion_mantenimiento" id="prestacion_mantenimiento"
            value="no"  
            @isset($patients)
            @if ($patient->prestacion_mantenimiento =='No')
            checked
            @else    
            @endif
            @endisset >
        <label class="form-check-label" for="">
            No
        </label>
    </div>


    <!--fin info paciente-->
