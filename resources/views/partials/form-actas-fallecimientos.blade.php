  
  <h2>ACTA DE FALLECIMIENTO</h2>
  <hr>
  <!-- Form Alta médica -->
  <h4>REGISTRO DE CADAVERES</h4>
  <div class="form-row">
      <div class="col-lg-auto">
        <div class="form-group col-md">
            <label for="">Turno:</label>
        </div>
        <div class="form-group ">
            <input type="text" tabindex="1" name="turno" id="turno"
                class="form-control"
                @isset($patient->turno)
                value="{{$patient->turno}}"
                @endisset>
        </div>
      </div>
      <div class="form-col-lg">
        <div class="form-group col-md">
            <label for="">Fecha:</label>
        </div>
        <div class="form-group ">
            <input type="date" tabindex="2" name="fecha" id="fecha"
                class="form-control"
                @isset($patient->fecha)
                value="{{$patient->fecha}}"
                @endisset>
        </div>
      </div>
      <div class="form-col-lg">
        <div class="form-group col-md">
            <label for="">Nicho:</label>
        </div>
        <div class="form-group ">
            <input type="text" tabindex="3" name="nicho" id="nicho"
                class="form-control"
                @isset($patient->nicho)
                value="{{$patient->nicho}}"
                @endisset>
        </div>
      </div>
  </div>


<div class="form-row">
    <div class="col-lg-auto ">
            <label for="">Nombre:</label>
        <input type="text" tabindex="4" name="nombre" id="nombre"
            class="form-control"
            @isset($patient->nombre)
            value="{{$patient->nombre}}"
            @endisset>
    </div>
    <div class="col-lg-3"></div>
    <div class="col-lg-auto ">
        <label for="">Sexo:</label>

        <input type="text" tabindex="5" name="sexo" id="sexo"
            class="form-control"
            @isset($patient->sexo)
            value="{{$patient->sexo}}"
            @endisset>
    </div>
</div>
<div class="form-row">
    <div class="form-group ">
            <label for="">Fecha de ingreso al hospital:</label>
        <input type="date" tabindex="6" name="fecha_ingreso" id="fecha_ingreso"
            class="form-control"
            @isset($patient->fecha_ingreso)
            value="{{$patient->fecha_ingreso}}"
            @endisset>
    </div>
    <div class="form-group ">
        <label for="">Hora:</label>

        <input type="time" tabindex="7" name="hora" id="hora"
            class="form-control"
            @isset($patient->hora_ingreso)
            value="{{$patient->hora_ingreso}}"
            @endisset>
    </div>
</div>
<div class="form-row">
    <div class="col-lg-auto ">
            <label for="">Edad:</label>
        <input type="tel" tabindex="8" name="edad" id="edad"
            class="form-control"
            @isset($patient->edad)
            value="{{$patient->edad}}"
            @endisset>
    </div>
    <div class="col-lg-auto">
        <label for="">Expediente:</label>

        <input type="text" tabindex="9" name="expediente" id="expediente"
            class="form-control"
            @isset($patient->expediente)
            value="{{$patient->expediente}}"
            @endisset>
    </div>
    <div class="col-lg-auto">
        <label for="">Fecha:</label>

        <input type="date" tabindex="10" name="fecha_expediente" id="fecha_expediente"
            class="form-control"
            @isset($patient->fecha_expediente)
            value="{{$patient->fecha_expediente}}"
            @endisset>
    </div>
</div>

<div class="form-row">
    <div class="col-lg-auto">
        <label for="">Sala:</label>
        <input type="text" tabindex="11" name="sala" id="sala"
            class="form-control"
            @isset($patient->sala)
            value="{{$patient->sala}}"
            @endisset>
    </div>   
    <div class="col-lg-auto">
        <label for="">Hora que falleció:</label>

        <input type="time" tabindex="12" name="hora_fallecimiento" id="hora_fallecimiento"
            class="form-control"
            @isset($patient->hora_fallecimiento)
            value="{{$patient->hora_fallecimiento}}"
            @endisset>
    </div>
</div>
<div class="form-row">
            <label>Procedencia:</label>
        <input type="text" tabindex="13" name="procedencia" id="procedencia"
            class="form-control"
            @isset($patient->procedencia)
            value="{{$patient->procedencia}}"
            @endisset>
  
</div>
<div class="form-row">
    <label>Nombre del padre:</label>
<input type="text" tabindex="13" name="nombre_padre" id="procedencia"
    class="form-control"
    @isset($patient->procedencia)
    value="{{$patient->procedencia}}"
    @endisset>

</div>
<div class="form-row">
    <label>Nombre de la madre:</label>
<input type="text" tabindex="14" name="nombre_madre" id="nombre_madre"
    class="form-control"
    @isset($patient->nombre_madre)
    value="{{$patient->nombre_madre}}"
    @endisset>

</div>
<div class="form-row">
    <label>Médico que certifica la defeunción:</label>
<input type="text" tabindex="15" name="medico" id="medico"
    class="form-control"
    @isset($patient->medico)
    value="{{$patient->medico}}"
    @endisset>

</div>
<div class="form-row">
    <label>Diagnóstico:</label>
    <textarea id="diagnostico" tabindex="16" name="diagnostico" class="form-control">
        @isset($patient->diagnostico)
        {{$patient->diagnostico}}
        @endisset
    </textarea>
</div>
<div class="form-row">
    <div class="col-lg-auto">
    <label>Empleado que reportó:</label>
    <input type="text" tabindex="17" name="empleado_que_reporta" id="empleado_que_reporta"
    class="form-control"
    @isset($patient->empleado_que_reporta)
    value="{{$patient->empleado_que_reporta}}"
    @endisset>
    </div>
    <div class="col-lg-auto">
        <label>Hora:</label>
    <input type="time" tabindex="18" name="hora_reporte_empleado" id="hora_reporte_empleado"
    class="form-control"
    @isset($patient->hora_reporte)
    value="{{$patient->hora_reporte}}"
    @endisset>
    </div>
</div>
<div class="form-row">
    <div class="col-lg-auto">
    <label>Cadaver recibido por:</label>
    <input type="text" tabindex="19" name="cadaver_recibido_por" id="cadaver_recibido_por"
    class="form-control"
    @isset($patient->cadaver_recibido_por)
    value="{{$patient->cadaver_recibido_por}}"
    @endisset>
    </div>
    <div class="col-lg-auto">
        <label>Hora:</label>
    <input type="time" tabindex="18" name="hora_recibido" id="hora_recibido"
    class="form-control"
    @isset($patient->hora_recibido)
    value="{{$patient->hora_recibido}}"
    @endisset>
    </div>
</div>

<hr>

<h3>USO EXCLUSIVO DEL QUE ENTREGA EL CADAVER</h3>
<div class="form-row">
    <div class="col-lg-auto">
    <label>Entregado por:</label>
    <input type="text" tabindex="19" name="cadaver_recibido_por" id="cadaver_recibido_por"
    class="form-control"
    @isset($patient->cadaver_recibido_por)
    value="{{$patient->cadaver_recibido_por}}"
    @endisset>
    </div>
    <div class="col-lg-auto">
        <label>Fecha:</label>
    <input type="date" tabindex="20" name="fecha_entrega_cadaver" id="fecha_entrega_cadaver"
    class="form-control"
    @isset($patient->fecha_entrega_cadaver)
    value="{{$patient->fecha_entrega_cadaver}}"
    @endisset>
    </div>
</div>
<div class="form-row">
    <div class="col-lg-auto">
    <label>Familiar que lo lleva:</label>
    <input type="text" tabindex="19" name="familiar_que_lleva" id="familiar_que_lleva"
    class="form-control"
    @isset($patient->familiar_que_lleva)
    value="{{$patient->familiar_que_lleva}}"
    @endisset>
    </div>
    <div class="col-lg-auto">
        <label>Hora:</label>
    <input type="time" tabindex="18" name="familiar_que_lleva_hora" id="familiar_que_lleva_hora"
    class="form-control"
    @isset($patient->familiar_que_lleva_hora)
    value="{{$patient->familiar_que_lleva_hora}}"
    @endisset>
    </div>
</div>
<div class="form-row">
    <div class="col-lg-auto">
    <label>Identidad Nro:</label>
    <input type="text" tabindex="19" name="numero_identidad" id="numero_identidad"
    class="form-control"
    @isset($patient->numero_identidad)
    value="{{$patient->numero_identidad}}"
    @endisset>
    </div>
    <div class="col-lg-auto">
        <label>Firma:</label>
    <input type="text" tabindex="20" name="firma" id="firma"
    class="form-control"
    @isset($patient->firma)
    value="{{$patient->firma}}"
    @endisset>
    </div>
</div>
<div class="form-row">
    <div class="form-group ">
        <label for="">Entregado a:</label>
        <input type="text" tabindex="21" name="entregado_a" id="entregado_a"
                class="form-control"
                @isset($patient->entregado_a)
                value="{{$patient->entregado_a}}"
                @endisset>
    </div>
</div>
<div class="form-row">
    <label>Observaciones:</label>
    <textarea id="observaciones" tabindex="22" name="observaciones" class="form-control">
        @isset($patient->observaciones)
        {{$patient->observaciones}}
        @endisset
    </textarea>
</div>

<hr>
<h3>USO EXCLUSIVO DE LA JEFATURA</h3>

    <div class="form-row ">
        <div class="col-lg-auto">
         
                <div class="form-group ">
                    <label for="">EL</label>
                    <input type="text" tabindex="23" name="el" id="el"
                        class="form-control"
                        @isset($patient->el)
                        value="{{$patient->el}}"
                        @endisset>
                </div>
        </div>
        <div class="col-lg-auto">         
                <div class="form-group ">
                    <label for="">DE</label>
                    <input type="text" tabindex="24" name="de" id="de"
                        class="form-control"
                        @isset($patient->de)
                        value="{{$patient->de}}"
                        @endisset>
                </div>
        </div>
        <div class="col-lg-auto">         
                <div class="form-group ">
                    <label for="">DE</label>
                    <input type="text" tabindex="25" name="de2" id="de2"
                        class="form-control"
                        @isset($patient->de2)
                        value="{{$patient->de2}}"
                        @endisset>
                </div>
        </div>
        </div>
    
   
    <!--fin info paciente-->
