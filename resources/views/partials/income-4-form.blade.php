<h5>Completar la información y seleccionar</h5>
                        <div class="form-row">
                            <label for="">Signos</label>

                        </div>
                       
                        <div class="form-row">
                            <div class="form-group col-md">
                                <input type="tel" tabindex="1" name="temperature" id="temperature" class="form-control"
                                    placeholder="Temperatura °C" 
                                    @isset($login_detail->temperature)
                                    disabled
                                    value="{{$login_detail->temperature}} °C"
                                    @endisset >
                            </div>

                            <div class="form-group col-md">
                                <input type="text" tabindex="1" name="weight" id="weight" class="form-control"
                                    placeholder="Peso Kg" 
                                    @isset($login_detail->weight)
                                    disabled
                                    value="{{$login_detail->weight}} Kg"
                                    @endisset >
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md">
                                <input type="text" tabindex="1" name="size" id="size" class="form-control"
                                    placeholder="Talla cm" 
                                    @isset($login_detail->size)
                                    disabled
                                    value="{{$login_detail->size}} cm"
                                    @endisset >
                            </div>

                            <div class="form-group col-md">
                                <input type="text" tabindex="1" name="blood_pressure" id="blood_pressure"
                                    class="form-control" placeholder="Presión Arterial mmHg" 
                                    @isset($login_detail->blood_pressure)
                                    disabled
                                    value="{{$login_detail->blood_pressure}} mmHg"
                                    @endisset >
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md">
                                <input type="text" tabindex="1" name="heart_rate" id="heart_rate" class="form-control"
                                    placeholder="Frecuencia Cardiaca ppm" 
                                    @isset($login_detail->heart_rate)
                                    disabled
                                    value="{{$login_detail->heart_rate}} ppm"
                                    @endisset >
                            </div>

                            <div class="form-group col-md">
                                <input type="text" tabindex="1" name="breathing_frequency" id="breathing_frequency"
                                    class="form-control" placeholder="Frecuencia Respiratoria rpm" 
                                    @isset($login_detail->breathing_frequency)
                                    disabled
                                    value="{{$login_detail->breathing_frequency}} rpm"
                                    @endisset >
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md">
                                <input type="text" tabindex="1" name="glucose" id="glucose" class="form-control"
                                    placeholder=" mg/dl
                        " @isset($login_detail->glucose)
                        disabled
                        value="{{$login_detail->glucose}} mg/dl"
                        @endisset
                         >
                            </div>

                            <div class="form-group col-md">
                                <input type="text" tabindex="1" name="oximetry" id="oximetry" class="form-control"
                                    placeholder="Oximetría PR BMP" 
                        @isset($login_detail->oximetry)
                        disabled
                        value="{{$login_detail->oximetry}} PR BMP"
                        @endisset
                         >
                            </div>
                        </div>
                                
                  
                        <div class="form-row">
                            <span>Sintomas</span>
                        </div>
                        <div class="form-row ml-1">
                            @isset($symptoms)
                            @foreach ($symptoms as $symptom)
                            <div class="col-lg-6 col-md-6 col-xs-6 form-check">

                                <input type="checkbox" class="form-check-input" name="symptom[]"
                                    value="{{$symptom->id}}"
                                    @isset($detail_symptoms)
                                    disabled

                                    @foreach ($detail_symptoms as $detail_symptom)
                                    @if($detail_symptom->symptom_id==$symptom->id)
                                    checked
                                    @endif
                                    @endforeach
                                   
                                    @endisset >
                                    
                                <label for="" class="form-check-label">{{$symptom->name}}</label>
                            </div>

                            @endforeach
                            @endisset
                        </div>
                        <!--fin info paciente-->
