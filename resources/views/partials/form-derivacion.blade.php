    <!--info paciente -->
    <div class="form-group ">
        <label for="">Tipo de derivación:</label>
    </div>
    <div class="form-row">
        <input class="form-check-input" type="text" onclick="change_identification_check(this);" name="tipo_derivacion" id="tipo_derivacion"
            placeholder="....." 
            @isset($patients)
            value="{{$patient->tipo_derivacion}}"
            @endisset>
    </div>

 <div class="form-row">
     <label for="">¿Afección inculpable?</label>
 </div>
    <div class="form-row">
        <div class="form-check">
            <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="afeccion_inculpable" id="afeccion_inculpable"
            value="si" 
            @isset($patients)
            @if ($patient->afeccion_inculpable=='si')
            checked
            @endif
            @else   
            checked
            @endisset
            >
        <label class="form-check-label" for="">
           si
        </label>
        </div>
       
        <div class="form-check">
            <input class="form-check-input" type="radio" onclick="change_identification_check(this);" name="afeccion_inculpable" id="afeccion_inculpable"
            value="no" 
            @isset($patients)
            @if ($patient->afeccion_inculpable=='si')
            checked
            @endif
            @else   
            checked
            @endisset
            >
        <label class="form-check-label" for="">
           no
        </label>
        </div>
 
    <!--fin info paciente-->
