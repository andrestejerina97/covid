          <div class="form-row col-md">
                            <label for="">Seleccionar enfermedad/es:</label>
                        </div>
                        <div class="form-row ml-1">

                            @isset($diseases)
                            @foreach ($diseases as $disease)
                            <div class="col-lg-6 col-md-6 col-xs-6 col-6 form-check">

                                <input type="checkbox" class="form-check-input" data-text="{{$disease->name}}" onchange="change_otra_enfermedad(this)" name="disease[]"
                                    value="{{$disease->id}}"
                                    @isset($patients)
                                    checked
                                    disabled
                                    @endisset >
                                <label class="form-check-label" for="defaultCheck1">
                                    {{$disease->name}}
                                </label>
                            </div>
                            <br>
                            @endforeach
                            @endisset
                        </div>
                        <div class="form-row" style="display: none"  id="input_disease_other">
                        <input type="text" class="form-control" name="disease_other" placeholder="Ingrese nombre de enfermedad">
                        </div>

                        <div class="form-row">
                            <label for="">Historial farmacológico</label>
                        </div>
                        <div class="form-row mb-2">
                            <textarea class="form-control" name="farm_history" id="farm_history"
                            @isset($patient->farm_history)
                            disabled
                            value="{{$patient->farm_history}}"
                            @endisset ></textarea>
                        </div>
                        <div class="form-row col-md mt-2">
                            <label for="">Antecedentes Alérgicos:</label>
                        </div>
                        <div class="form-row ml-1">
                            @isset($allergic_histories)
                            @foreach ($allergic_histories as $allergic_history)
                            <div class="col-lg-6 col-md-6 col-xs-6 col-6 form-check">

                                <input type="checkbox" class="form-check-input" data-text="{{$allergic_history->name}}" onchange="change_otra_alergia(this)" name="disease[]"
                                    value="{{$allergic_history->id}}"
                                    @isset($patients)
                                    checked
                                    disabled
                                    @endisset >
                                <label class="form-check-label" for="defaultCheck1">
                                    {{$allergic_history->name}}
                                </label>
                            </div>
                            <br>
                            @endforeach
                            @endisset
                        </div>
                        <div class="form-row" style="display: none"  id="input_allergic_other">
                            <input type="text" class="form-control" name="allergic_other" placeholder="Ingrese nombre de alergia">
                            </div>
                     