<div class="form-group col-md">
    <label for="">Clasificación TRIAGE:</label>
</div>
<div class="form-check" style="background-color: #ff0000">
    <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
        value="ff0000"  data-triage="ff0000"
        @isset($patient->triage)
        @if ($patient->triage=="ff0000")
            checked
        @else
        disabled
        @endif
        
        @endisset  >
    <label class="form-check-label" for="exampleRadios1">
        Estado Critico o Descompensado
    </label>
</div>
<div class="form-check" style="background-color: #ee8625">
    <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
        value="ee8625"
        @isset($patient->triage)
        @if ($patient->triage=="ee8625")
        checked
        @else
        disabled
     @endif
        @endisset >
    <label class="form-check-label" for="exampleRadios2">
        Emergencia
    </label>
</div>
<div class="form-check" style="background-color: #e7ff0afb">
    <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
        value="e7ff0afb"
        @isset($patient->triage)
        @if ($patient->triage=="e7ff0afb")
        checked
        @else
        disabled
        @endif
      
        @endisset >
    <label class="form-check-label" for="exampleRadios1">
        Urgencia
    </label>
</div>
<div class="form-check" style="background-color: #1fc23a">
    <input  onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
        value="1fc23a"
        @isset($patient->triage)
        @if ($patient->triage=="1fc23a")
        checked
        @else
        disabled
        @endif
      
        @endisset >
    <label class="form-check-label" for="exampleRadios2">
        Urgencias Menores
    </label>
</div>
<div class="form-check" style="background-color: #c9b6b6">
    <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
        value="c9b6b6"
        @isset($patient->triage)
        @if ($patient->triage=="c9b6b6")
        checked
        @else
        disabled
        @endif
  
        @endisset >
    <label class="form-check-label" for="exampleRadios2">
        Fallecido
    </label>
</div>
<div class="form-check" style="background-color: #392ed1">
    <input onclick="change_checked_triage(this)" class="form-check-input triages" type="radio" name="triage" id="triage"
        value="392ed1" 
        @isset($patient->triage)
        @if ($patient->triage=="392ed1")
        checked
        @else
        disabled    
        @endif    

        @endisset >
    <label class="form-check-label" for="exampleRadios2">
        Alta Hospitalaria
    </label>
</div>

