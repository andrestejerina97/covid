<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class details_clinical_historial extends Model
{
    protected $guarded=['created_at','modified_at'];
    protected $table="details_clinical_historial";
}
