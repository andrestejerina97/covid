<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clinical_history extends Model
{
    protected $table = 'clinical_histories';
    protected $guarded = ['id'];
}
