<?php

namespace App\Http\Controllers;

use App\acta_fallecidos;
use Illuminate\Http\Request;

class ActaFallecidosController extends Controller
{
   public function index($id)
   {
     return view("public.acta-fallecimiento")
       ->with('id',$id);
   }
   public function store(Request $request)
   {
     acta_fallecidos::insert($request->except('_token'));
     return redirect()->route('search');

   }
}
