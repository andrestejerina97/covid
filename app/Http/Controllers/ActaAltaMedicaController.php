<?php

namespace App\Http\Controllers;

use App\acta_alta_medica;
use App\acta_fallecidos;
use Illuminate\Http\Request;

class ActaAltaMedicaController extends Controller
{
    public function store(Request $request)
    {
        acta_alta_medica::insert($request->except('_token'));
        return redirect()->route('search');
    }
    public function index($id)
    {
        return view("public.alta-medica")
        ->with('id',$id);
    }
}
