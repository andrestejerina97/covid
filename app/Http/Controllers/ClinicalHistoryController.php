<?php

namespace App\Http\Controllers;

use App\clinical_history;
use App\detail_symptoms;
use App\details_clinical_historial;
use App\details_diseases;
use App\details_symptoms_historial;
use App\diseases;
use App\institutions_source;
use App\login_details;
use App\symptoms;
use Illuminate\Http\Request;

class ClinicalHistoryController extends Controller
{
    public function index($id)
    {
        
        $clinical_histories=clinical_history::find($id);

        $diseases=details_diseases::where('clinical_histories_id','=',$id)
        ->join('diseases','details_diseases.disease_id','=','diseases.id')->get();
        $detail_symptoms=detail_symptoms::join('symptoms','symptoms.id','=','detail_symptoms.symptom_id')
        ->where('clinical_histories_id','=',$id)
        ->select('symptoms.*')->get();
        $clinical_histories= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->join('details_tests','details_tests.id','=','clinical_histories.details_test_id')
        ->join('details_results','details_results.id','=','clinical_histories.details_result_id')
        ->join('institutions','institutions.id','=','clinical_histories.institution_id')
        ->select('clinical_histories.*','patients.*','institutions.*','details_tests.*','details_results.*')
        ->where('clinical_histories.id' ,'=',$id)
        ->orderBy('clinical_histories.id','asc')
        ->get();
        $symptoms=symptoms::all();

        $detail_symptoms= detail_symptoms::join('symptoms','symptoms.id','=','detail_symptoms.symptom_id')
        ->where('detail_symptoms.clinical_histories_id','=',$id)
        ->select('symptoms.*','detail_symptoms.*')->get();
        $login_details=login_details::where('clinical_histories_id','=',$id)->get();

        $details_history_clinicals=details_clinical_historial::join('clinical_histories','clinical_histories.id','details_clinical_historial.clinical_histories_id')
        ->where('details_clinical_historial.clinical_histories_id',$id)
        ->select('details_clinical_historial.*')->get();
        $detail_symptoms_historial=details_symptoms_historial::join('details_clinical_historial','details_clinical_historial.id','details_symptoms_historials.details_historial_id')
        ->join('clinical_histories','clinical_histories.id','details_clinical_historial.clinical_histories_id')
        ->where('details_clinical_historial.clinical_histories_id',$id)
        ->select('details_symptoms_historials.*')
        ->get();
        $institutes= institutions_source::all();
        return view('public.clinical-history-index')
        ->with('patients', $clinical_histories)
        ->with('diseases', $diseases)
        ->with('detail_symptoms_historial',$detail_symptoms_historial)
        ->with('details_history_clinicals',$details_history_clinicals)
        ->with('login_details', $login_details)
        ->with('institutes',$institutes)
        ->with('id',$id)
        ->with('detail_symptoms', $detail_symptoms)
        ->with('symptoms', $symptoms);
        
    }

    public function update(Request $request)
    {
        //dd($request->except("symptoms","_token",'id'));
        $data=$request->except("symptom","_token",'id');
        $details_clinical_historial=details_clinical_historial::create($data);
        
        if($request->has('symptom')){ 
        foreach ($request->input('symptom') as $key => $val) 
            {
            $detail_symptoms = new details_symptoms_historial();
            if (in_array($val, $request->input('symptom')))
            {
                $detail_symptoms->symptom_id = $val;
                $detail_symptoms->details_historial_id =$details_clinical_historial->id;
                $detail_symptoms->save();
            }
            }
        }
            $clinical_histories=clinical_history::find($request->input("clinical_histories_id"));
            $clinical_histories->triage=$data['triage'];
            $clinical_histories->save();
            return redirect()->route("search");


    }

    public function edit(Request $request)
    {
        $symptoms=symptoms::all();

        return view('public.clinical-history-edit')
        ->with('symptoms', $symptoms)
        ->with('id', $request->input("id"));
    }

    public function clinical_record($clinical_histories_id)
    {   
        $id=$clinical_histories_id;
        $clinical_histories=clinical_history::find($id);

        $diseases=details_diseases::where('clinical_histories_id','=',$id)
        ->join('diseases','details_diseases.disease_id','=','diseases.id')->get();
        $detail_symptoms=detail_symptoms::join('symptoms','symptoms.id','=','detail_symptoms.symptom_id')
        ->where('clinical_histories_id','=',$id)
        ->select('symptoms.*')->get();
        $clinical_histories= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->join('details_tests','details_tests.id','=','clinical_histories.details_test_id')
        ->join('details_results','details_results.id','=','clinical_histories.details_result_id')
        ->join('institutions','institutions.id','=','clinical_histories.institution_id')
        ->select('clinical_histories.*','patients.*','institutions.*','details_tests.*','details_results.*')
        ->where('clinical_histories.id' ,'=',$id)
        ->orderBy('clinical_histories.id','asc')
        ->get();
        $symptoms=symptoms::all();

        $detail_symptoms= detail_symptoms::join('symptoms','symptoms.id','=','detail_symptoms.symptom_id')
        ->where('detail_symptoms.clinical_histories_id','=',$id)
        ->select('symptoms.*','detail_symptoms.*')->get();
        $login_details=login_details::where('clinical_histories_id','=',$id)->get();

        $details_history_clinicals=details_clinical_historial::where('clinical_histories_id',$id)->get();
        $institutes= institutions_source::all();
        return view('public.clinical-history-edit')
        ->with('patients', $clinical_histories)
        ->with('diseases', $diseases)
        ->with('details_history_clinicals',$details_history_clinicals)
        ->with('institutes',$institutes)
        ->with('id',$id)
        ->with('symptoms', $symptoms);
       
    }
    public function clinical_record_dead($clinical_histories_id)
    {   
        $id=$clinical_histories_id;
        $clinical_histories=clinical_history::find($id);

        $diseases=details_diseases::where('clinical_histories_id','=',$id)
        ->join('diseases','details_diseases.disease_id','=','diseases.id')->get();
        $detail_symptoms=detail_symptoms::join('symptoms','symptoms.id','=','detail_symptoms.symptom_id')
        ->where('clinical_histories_id','=',$id)
        ->select('symptoms.*')->get();
        $clinical_histories= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->join('details_tests','details_tests.id','=','clinical_histories.details_test_id')
        ->join('details_results','details_results.id','=','clinical_histories.details_result_id')
        ->join('institutions','institutions.id','=','clinical_histories.institution_id')
        ->select('clinical_histories.*','patients.*','institutions.*','details_tests.*','details_results.*')
        ->where('clinical_histories.id' ,'=',$id)
        ->orderBy('clinical_histories.id','asc')
        ->get();
        $symptoms=symptoms::all();

        $detail_symptoms= detail_symptoms::join('symptoms','symptoms.id','=','detail_symptoms.symptom_id')
        ->where('detail_symptoms.clinical_histories_id','=',$id)
        ->select('symptoms.*','detail_symptoms.*')->get();
        $login_details=login_details::where('clinical_histories_id','=',$id)->get();

        $details_history_clinicals=details_clinical_historial::where('clinical_histories_id',$id)->get();
        $institutes= institutions_source::all();
        return view('public.clinical-history-edit-dead')
        ->with('patients', $clinical_histories)
        ->with('diseases', $diseases)
        ->with('details_history_clinicals',$details_history_clinicals)
        ->with('institutes',$institutes)
        ->with('id',$id)
        ->with('symptoms', $symptoms);
       
    }
}
