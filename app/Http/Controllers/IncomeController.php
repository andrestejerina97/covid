<?php

namespace App\Http\Controllers;

use App\allergic_history;
use App\clinical_history;
use App\detail_allergic_history;
use App\detail_symptoms;
use App\details_diseases;
use App\details_results;
use App\details_sign;
use App\details_test;
use App\diseases;
use App\institutions;
use App\login_details;
use App\patients;
use App\symptoms;
use Illuminate\Http\Request;

class IncomeController extends Controller
{
public function index()
{
    $diseases=diseases::all();
    $symptoms=symptoms::all();
    $allergic_histories= allergic_history::all();
    
    return view('public.income',compact('diseases','symptoms','allergic_histories'));
}


public function store(Request $request)
{

 $patient= new patients();
 $patient->proceedings_number=$request->input('proceedings_number');
 $patient->first_name=$request->input('first_name');
 $patient->second_name=$request->input('second_name');
 $patient->first_lastname=$request->input('first_lastname');
 $patient->second_lastname=$request->input('second_lastname');
 $patient->birhdate=$request->input('birhdate');
 $patient->age=$request->input('age');
 $patient->gender=$request->input('gender');

 $patient->occupation=$request->input('occupation');
 $patient->home=$request->input('home');
 $patient->place_birth=$request->input('place_birth');
 $patient->person_charge=$request->input('person_charge');
 $patient->blood_type=$request->input('blood_type');
 $patient->cellphone=$request->input('cellphone');
 $patient->save();
 if( $patient->proceedings_number == '' or $patient->proceedings_number == null){
  $patient->proceedings_number = 'desconocido-'.$patient->id;
  $patient->update();
 }
 $institution=new institutions();
 $institution->institution_source_id=$request->input('institution_source');
 $institution->name=$request->input('institution_name');
 $institution->save();


$details_tests= new details_test();
$details_tests->test_type=$request->input('test_type');
$details_tests->place=$request->input('place');
$details_tests->date=$request->input('date');
$details_tests->result=$request->input('result');
$details_tests->delivery_date=$request->input('delivery_date');
$details_tests->save();

$details_results=new details_results();
$details_results->diagnosis=$request->input('diagnosis');
$details_results->cause=$request->input('cause');
$details_results->treatment=$request->input('treatment');
$details_results->process=$request->input('process');
$details_results->responsible_doctor=$request->input('responsible_doctor');
$details_results->save(); 

$clinical_history=new clinical_history();
$clinical_history->patient_id=$patient->id;
$clinical_history->details_result_id=$details_results->id;
$clinical_history->details_test_id=$details_tests->id;
$clinical_history->institution_id=$institution->id;
$clinical_history->triage=$request->input('triage');
$clinical_history->save();


if ($request->has('disease')) {
  foreach ($request->input('disease') as $key => $val) 
{
 $details_diseases = new details_diseases();
  if (in_array($val, $request->input('disease')))
   {
     $details_diseases->clinical_histories_id = $clinical_history->id;
     $details_diseases->disease_id = $val;
     $details_diseases->save();
   }
}
}


if($request->has('disease_other')){
  $disasesNew=new diseases();
  $disasesNew->name=$request->input('disease_other');
  $disasesNew->save();
   $details_diseases = new details_diseases();
   $details_diseases->clinical_histories_id = $clinical_history->id;
   $details_diseases->disease_id = $disasesNew->id;
   $details_diseases->save();
   
 }
if ($request->has('allergic_other')){
  $alergic=new allergic_history();
  $alergic->name=$request->input('allergic_other');
  $alergic->save();
   $alergic_other = new detail_allergic_history();
   $alergic_other->allergic_histories_id = $alergic->id;
   $alergic_other->clinical_histories_id = $clinical_history->id;
   $alergic_other->save();
}

if ($request->has('symptom')) {
  
foreach ($request->input('symptom') as $key => $val) 
{
 $detail_symptoms = new detail_symptoms();
  if (in_array($val, $request->input('symptom')))
   {
     $detail_symptoms->symptom_id = $val;
     $detail_symptoms->clinical_histories_id = $clinical_history->id;
     $detail_symptoms->save();
   }
}
}

$login_details= new login_details();
$login_details->farm_history=$request->input('farm_history');
$login_details->temperature=$request->input('temperature');
$login_details->weight=$request->input('weight');
$login_details->blood_pressure=$request->input('blood_pressure');
$login_details->heart_rate=$request->input('heart_rate');
$login_details->breathing_frequency=$request->input('breathing_frequency');
$login_details->glucose=$request->input('glucose');
$login_details->oximetry=$request->input('oximetry');
$login_details->size=$request->input('size');
$login_details->clinical_histories_id=$clinical_history->id;
$login_details->save();

switch ($request->input('triage')) {
  case 'c9b6b6':
    return redirect()->route('fallecimiento',['id'=>$clinical_history->id]);
  break;
  case '392ed1':
    return redirect()->route('alta',['id'=>$clinical_history->id]);
  break;
  
  default:
  return redirect()->route('search');

    break;
}
 
}
}
