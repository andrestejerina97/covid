<?php

namespace App\Http\Controllers;

use App\clinical_history;
use Illuminate\Http\Request;

class SearchController extends Controller
{
  public function index()
  {
        $clinical_histories= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->join('details_tests','details_tests.id','=','clinical_histories.details_test_id')
        ->join('details_results','details_results.id','=','clinical_histories.details_result_id')
        ->join('institutions','institutions.id','=','clinical_histories.institution_id')
        ->select('clinical_histories.*','patients.first_name','patients.second_name','patients.first_lastname','patients.second_lastname')
        ->orderBy('clinical_histories.id','desc')
        ->paginate(10);    
        return view('public.search')
        ->with('clinical_histories', $clinical_histories);
    }
    public function filter(Request $request)
    {
      
      $column_filter='patients.proceedings_number';
      $clinical_histories= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
      ->join('details_tests','details_tests.id','=','clinical_histories.details_test_id')
      ->join('details_results','details_results.id','=','clinical_histories.details_result_id')
      ->join('institutions','institutions.id','=','clinical_histories.institution_id')
      ->where($column_filter,'like','%'.$request->input('filter').'%')
      ->select('clinical_histories.*','patients.first_name','patients.second_name','patients.first_lastname','patients.second_lastname')
      ->orderBy('clinical_histories.id','desc')
      ->paginate(10);    
      return view('public.search')
      ->with('clinical_histories', $clinical_histories);
    }
    
}
