<?php

namespace App\Http\Controllers;

use App\clinical_history;
use Illuminate\Http\Request;

class ChartsController extends Controller
{
  public function index()
  {
      for ($i=0; $i < 9; $i++) {
        if($i==8){
          $range= "+".(string)(($i)*10);
          $limiteInferior= $i  *10 ;
          $limiteSuperior=($i+1) * 100;
        }else{
          $range= (string)($i *10)."-".(string)(($i+1)*10);
          $limiteInferior= $i  *10 ;
          $limiteSuperior=($i+1) * 10;
        }
        $rangeage[$range]= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->select('clinical_histories.*')
        ->where('patients.age','>',$limiteInferior)
        ->where('patients.age','<',$limiteSuperior)
        ->get()
        ->count();
      }
        $rangeage[$range]= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->select('clinical_histories.*')
        ->where('patients.age','>',$limiteInferior)
        ->where('patients.age','<',$limiteSuperior)
        ->get()
        ->count();
      
   
        $gender['masculino']= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->select('clinical_histories.*')
        ->where('patients.gender','=','masculino')
        ->get()
        ->count();
        $gender['femenino']= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->select('clinical_histories.*')
        ->where('patients.gender','=','femenino')
        ->get()
        ->count();
        $diagnostic['recuperados']= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->select('clinical_histories.*')
        ->where('clinical_histories.triage','=','392ed1')
        ->get()
        ->count();
        $diagnostic['fallecidos']= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->select('clinical_histories.*')
        ->where('clinical_histories.triage','=','c9b6b6')
        ->get()
        ->count();
        $diagnostic['contagiados']= clinical_history::join('patients','patients.id','=','clinical_histories.patient_id')
        ->select('clinical_histories.*')
        ->where('clinical_histories.triage','<>','c9b6b6')
        ->where('clinical_histories.triage','<>','392ed1')
        ->get()
        ->count();

      

      return view('public.charts',compact('rangeage','gender','diagnostic'));

    }
}
