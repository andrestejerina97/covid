<?php

namespace App\Http\Controllers;

use App\institutions_source;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    public function getInstitute(Request $request)
    {
    $institutes=institutions_source::where('departament','=',$request->input('departament'))->get();
    return new JsonResponse(['institutes' => $institutes]);

    }

    public function alta()
    {
        return view('public.alta-medica');
    }
    public function baja()
    {
        return view('public.acta-fallecimiento');
    }
}
