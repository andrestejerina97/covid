<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(DiseaseSeeder::class);  
        $this->call(SymptomsSeeder::class);  
        $this->call(InstitutionsSeeder::class);  

    }
}
