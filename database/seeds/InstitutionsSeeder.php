<?php

use App\institutions_source;
use Illuminate\Database\Seeder;

class InstitutionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $institutions_source= new institutions_source();
      $institutions_source->name="Hospital General San Felipe";
      $institutions_source->departament="Francisco Morazan";
      $institutions_source->save();

      $institutions_source= new institutions_source();
      $institutions_source->name="Instituto cardiopulmonar (Hospital del torax)";
      $institutions_source->departament="Francisco Morazan";

      $institutions_source->save();

      $institutions_source= new institutions_source();
      $institutions_source->name="Instituto nacional de seguro social Francisco Morazan";
      $institutions_source->departament="Francisco Morazan";

      $institutions_source->save();

      $institutions_source= new institutions_source();
      $institutions_source->name="Hospital Catarino Rivas";
      $institutions_source->departament="Cortes";
      $institutions_source->save();

      $institutions_source= new institutions_source();
      $institutions_source->name="Hospital Leonardo Martinez";
      $institutions_source->departament="Cortes";
      $institutions_source->save();

      $institutions_source= new institutions_source();
      $institutions_source->name="Instituto Nacional de Seguro Social del Valle de Sula";
      $institutions_source->departament="Cortes";
      $institutions_source->save();
    }
}
