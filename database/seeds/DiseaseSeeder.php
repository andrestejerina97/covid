<?php

use App\allergic_history;
use App\diseases;
use Illuminate\Database\Seeder;

class DiseaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $disease=new diseases();
        $disease->name="Tuberculosis";
        $disease->save();

        $disease=new diseases();
        $disease->name="Fiebre Tifoidea";   
        $disease->save();
                                  
        $disease=new diseases();
        $disease->name="ETS";  
        $disease->save();
                     
        $disease=new diseases();
        $disease->name="Hepatitis";
        $disease->save();
                 
        $disease=new diseases();
        $disease->name="Paludismo";
        $disease->save();
               
        $disease=new diseases();
        $disease->name="Neumonia"; 
        $disease->save();
                                   
        $disease=new diseases();
        $disease->name="Sinusitis"; 
        $disease->save();
                                    
        $disease=new diseases();
        $disease->name="Fiebre Reumatica";
        $disease->save();

        $disease=new diseases();
        $disease->name="Pancreatitis";
        $disease->save();

        $disease=new diseases();
        $disease->name="Parasitismo";
        $disease->save();

        $disease=new diseases();
        $disease->name="Hepilepsia";
        $disease->save();

        $disease=new diseases();
        $disease->name="Obesidad";
        $disease->save();

        $disease=new diseases();
        $disease->name="Gastritis";
        $disease->save();

        $disease=new diseases();
        $disease->name="Cancer";
        $disease->save();

        $disease=new diseases();
        $disease->name="Ulcera Peptica";
        $disease->save();

        $disease=new diseases();
        $disease->name="Diabetes";
        $disease->save();

        $disease=new diseases();
        $disease->name="Artritis";
        $disease->save();

        $disease=new diseases();
        $disease->name="Anemia";
        $disease->save();

        $disease=new diseases();
        $disease->name="Dengue";
        $disease->save();

        $disease=new diseases();
        $disease->name="HTA";
        $disease->save();
        
        $disease=new diseases();
        $disease->name="Otros";
        $disease->save();

        allergic_history::insert([
            'name'=>"PENICILINA"]);
       
        allergic_history::insert([
            'name'=>"SALICILATOS"]);
        allergic_history::insert([
            'name'=>"CORTICOIDES"]);
        allergic_history::insert([
            'name'=>"ANTIÁCIDOS"]);
        allergic_history::insert([
            'name'=>"ANTIBIOTICOS"]);
        allergic_history::insert([
            'name'=>"LAXANTES"]); 
        allergic_history::insert([
            'name'=>"ANESTESIA"]);
        allergic_history::insert([
            'name'=>"OTROS"]);  
  
  }
}
