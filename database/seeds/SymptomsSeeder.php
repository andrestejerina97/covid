<?php

use App\symptoms;
use Illuminate\Database\Seeder;

class SymptomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $symptom= new symptoms();
        $symptom->name= "Fiebre";   
        $symptom->save();
                       
       $symptom= new symptoms();
        $symptom->name= 'Tos'; 
         $symptom->save();
                              
       $symptom= new symptoms();
        $symptom->name= 'Cefalea';
          $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Mialgia';
           $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Dolor de Garganta';
            $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Escalofríos';
             $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Conjuntivitis';
              $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Anosmia';
               $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Fatiga';
                $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Diarrea';
                 $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Nauseas';
                  $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Vomito';
                   $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Hemoptisis';
                   $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Disnea';
                   $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Labios/dedos azules';
                   $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Dolor opresivo en el pecho';
                   $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Confusión';
                   $symptom->save();
       
       $symptom= new symptoms();
        $symptom->name= 'Convulsiones';
                   $symptom->save();
       
    }
}
