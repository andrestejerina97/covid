<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
      /*  $role = new Role();
        $role->name = 'admin';
        $role->description = 'Administrator';
        $role->save();       
        $role = new Role();
        $role->name = 'user';
        $role->description = 'User';
        $role->save();
        $role = new Role();
        $role->name = 'enfermero';
        $role->description = 'Enfermero';
        $role->save();
        $user= User::create([
            'name' =>'administrador',
            'email' => 'lubelomo@gmail.com',
            'password' => Hash::make('Luislopez1@'),
        ]);
        
        $user->roles()->attach(Role::where('name','admin')->first());*/
        $role = new Role();
        $role->name = 'viceministro';
        $role->description = 'Viceministro';
        $role->save();  
        $role = new Role();
        $role->name = 'ministro';
        $role->description = 'Ministro';
        $role->save();
        $role = new Role();
        $role->name = 'presidente';
        $role->description = 'Presidente';
        $role->save();  
        
    }
}
