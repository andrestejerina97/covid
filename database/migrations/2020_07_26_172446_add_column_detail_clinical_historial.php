<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDetailClinicalHistorial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('details_clinical_historial', function (Blueprint $table) {
            $table->string("observations")->nullable();
            $table->string("time")->nullable();
            $table->string("name")->nullable();
            $table->string("lastname")->nullable();

           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('details_clinical_historial', function (Blueprint $table) {
            $table->dropColumn("observations");
            $table->dropColumn("time");
            $table->dropColumn("name");
            $table->dropColumn("lastname");

           });
    }
}
