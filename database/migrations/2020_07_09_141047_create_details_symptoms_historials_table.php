<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsSymptomsHistorialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details_symptoms_historials', function (Blueprint $table) {
            $table->foreignId("symptom_id")->constrained("symptoms")->onDelete("cascade")->onUpdate("cascade");
            $table->foreignId("details_historial_id")->constrained("details_clinical_historial")->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details_symptoms_historials');
    }
}
