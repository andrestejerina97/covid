<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsClinicalHistorialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details_clinical_historial', function (Blueprint $table) {
            $table->id();
            $table->foreignId("clinical_histories_id")->constrained("clinical_histories")->onDelete("cascade")->onUpdate("cascade");
            $table->string('temperature')->nullable();
            $table->string('weight')->nullable();
            $table->string('size')->nullable();
            $table->string('blood_pressure')->nullable();
            $table->string('heart_rate')->nullable();
            $table->string('breathing_frequency')->nullable();
            $table->string('glucose')->nullable();
            $table->string('oximetry')->nullable();
            $table->string('farm_history')->nullable();
            $table->string('triage')->nullable();
            $table->string('test_type')->nullable();
            $table->string('place')->nullable();
            $table->string('date')->nullable();
            $table->string('result')->nullable();
            $table->date('delivery_date')->nullable();

            $table->string('diagnosis')->nullable();
            $table->string('cause')->nullable();
            $table->string('treatment')->nullable();
            $table->string('process')->nullable();
            $table->string('responsible_doctor')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details_clinical_historials');
    }
}
