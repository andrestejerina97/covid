<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
             //campos extra
             $table->string('proceedings_number')->nullable();
             $table->string('first_name')->nullable();
             $table->string('second_name')->nullable();
             $table->string('first_lastname')->nullable();
             $table->string('second_lastname')->nullable();
             $table->string('birhdate')->nullable();
             $table->string('age')->nullable();
             $table->string('gender')->nullable();
             $table->string('occupation')->nullable();
             $table->string('home')->nullable();
             $table->string('place_birth')->nullable();
             $table->string('person_charge')->nullable();
             $table->string('blood_type')->nullable();
             $table->string('cellphone')->nullable();

             //fin campos extra
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
