<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details_results', function (Blueprint $table) {
            $table->id();
            $table->string('diagnosis')->nullable();
            $table->string('cause')->nullable();
            $table->string('treatment')->nullable();
            $table->string('process')->nullable();
            $table->string('responsible_doctor')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details_results');
    }
}
