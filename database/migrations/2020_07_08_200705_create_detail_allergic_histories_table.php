<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailAllergicHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_allergic_histories', function (Blueprint $table) {
         //  $table->unsignedBigInteger('clinical_histories_id');
            $table->unsignedBigInteger('allergic_histories_id')->nullable();
            $table->foreignId("clinical_histories_id")->constrained("clinical_histories")->onDelete("cascade")->onUpdate("cascade");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('detail_allergic_histories');

    }
}
