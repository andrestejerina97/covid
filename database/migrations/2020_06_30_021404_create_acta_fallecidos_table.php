<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActaFallecidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actas_fallecidos', function (Blueprint $table) {
            $table->id();
            $table->string('fecha')->nullable();
            $table->string('turno')->nullable();
            $table->string('nicho')->nullable();
            $table->string('sexo')->nullable();
            $table->string('hora')->nullable();
            $table->string('hora_reporte_empleado')->nullable();

            $table->string('nombre')->nullable();
            $table->string('fecha_ingreso')->nullable();
            $table->string('hora_ingreso')->nullable();
            $table->string('edad')->nullable();
            $table->string('expediente')->nullable();
            $table->string('fecha_expediente')->nullable();
            $table->string('sala')->nullable();
            $table->string('hora_fallecimiento')->nullable();
            $table->string('procedencia')->nullable();
            $table->string('nombre_padre')->nullable();
            $table->string('nombre_madre')->nullable();
            $table->string('medico')->nullable();
            $table->string('diagnostico')->nullable();
            $table->string('empleado_que_reporta')->nullable();
            $table->string('hora_reporte')->nullable();
            $table->string('hora_recibido')->nullable();
            $table->string('cadaver_recibido_por')->nullable();
            $table->string('fecha_entrega_cadaver')->nullable();
            $table->string('familiar_que_lleva')->nullable();
            $table->string('numero_identidad')->nullable();
            $table->string('familiar_que_lleva_hora')->nullable();
            $table->string('firma')->nullable();
            $table->string('entregado_a')->nullable();
            $table->string('observaciones')->nullable();
            $table->string('el')->nullable();
            $table->string('de')->nullable();
            $table->string('de2')->nullable();
            $table->foreignId("clinical_histories_id")->constrained("clinical_histories")->onDelete("cascade")->onUpdate("cascade");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actas_fallecidos');
    }
}
