<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActaAltaMedicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actas_alta_medica', function (Blueprint $table) {
            $table->id();
            $table->string("nombre_trabajador")->nullable();
            $table->string("calle_trabajador")->nullable();
            $table->string("numero_trabajador")->nullable();
            $table->string("piso_trabajador")->nullable();
            $table->string("departamento_trabajador")->nullable();
            $table->string("localidad_trabajador")->nullable();
            $table->string("provincia_trabajador")->nullable();
            $table->string("cp_trabajador")->nullable();
            $table->string("tel_trabajador")->nullable();
            $table->string("tel_movil_trabajador")->nullable();

            $table->string("establecimiento_asistencial")->nullable();
            $table->string("nombre_completo_prestador")->nullable();
            $table->string("fecha_resultado_covid")->nullable();
            $table->string("hora_resultado_covid")->nullable();
            $table->string("fecha_prueba_covid")->nullable();
            $table->string("hora_prueba_covid")->nullable();
            $table->string("fecha_primera_atencion")->nullable();
            $table->string("hora_primera_atencion")->nullable();
            $table->text("descripcion_consulta_motivo")->nullable();
            $table->text("indicaciones")->nullable();

            $table->string('tratamiento_pendiente')->nullable();
            $table->string('nombre_tratamiento')->nullable();
            $table->string('fecha_proxima_revision')->nullable();
            $table->string('hora_proxima_revision')->nullable();
            $table->string('recalificacion_profesional')->nullable();
            $table->string('fecha_retorno_trabajo')->nullable();
            $table->string('hora_retorno_trabajo')->nullable();
            $table->string('fecha_fin_tratamiento')->nullable();
            $table->string('hora_fin_tratamiento')->nullable();
            $table->string('motivo_de_cese')->nullable();
            $table->string('tipo_derivacion')->nullable();
            $table->string('aleccion_inculpable')->nullable();
            $table->string('secuela_incapacitante')->nullable();
            $table->string('prestacion_mantenimiento')->nullable();
            $table->string('medico')->nullable();
            $table->string('diagnostico')->nullable();
            $table->string('empleado_que_reporta')->nullable();
            $table->string('hora_reporte')->nullable();
            $table->string('hora_recibido')->nullable();
            $table->string('cadaver_recibido_por')->nullable();
            $table->foreignId("clinical_histories_id")->constrained("clinical_histories")->onDelete("cascade")->onUpdate("cascade");

            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actas_alta_medica');
    }
}
