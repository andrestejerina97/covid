<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
define('STDIN',fopen("php://stdin","r"));
Route::get('install', function() {
    Artisan::call('migrate',['--force'=>true]);
});
*/
Route::get('/','HomeController@index')->name('inicio');


// nuevo ingreso
Route::get('/nuevo-ingreso', 'IncomeController@index')->name('nuevoingreso');
Route::post('/store-ingreso', 'IncomeController@store')->name('store.ingreso');
Route::get('/get-ingreso/{idIngreso}', 'ClinicalHistoryController@index')->name('update.ingreso.get');
Route::post('/edit-ingreso', 'ClinicalHistoryController@edit')->name('edit.ingreso');
Route::post('/update-ingreso', 'ClinicalHistoryController@update')->name('update.ingreso');
Route::get('/nuevo-ficha-clinica/{clinical_history_id}', 'ClinicalHistoryController@clinical_record')->name('new.clinical_record');
Route::get('/nuevo-ficha-fallecido/{clinical_history_id}', 'ClinicalHistoryController@clinical_record_dead')->name('new.clinical_record_dead');


Route::get('/instituciones-por-departamento', 'HomeController@getInstitute')->name('get.intitute');

//
//Search
Route::get('/buscar-pacientes', 'SearchController@index')->name('search');
Route::post('/buscar-pacientes-por', 'SearchController@filter')->name('search.patient');
//End Search

//charts
Route::get('/resumenes', 'ChartsController@index')->name('charts');

//end charts
Route::get('/home', 'HomeController@index')->name('home');

//users admin
Route::get('/admin-user', 'UserController@index')->name('admin.users');
Route::get('/admin-user-filter', 'UserController@index')->name('admin.users.filter');
Route::post('/admin-user-delete', 'UserController@delete')->name('admin.users.delete');
Route::post('/admin-user-update', 'UserController@update')->name('admin.users.update');
Route::post('/admin-user-edit', 'UserController@edit')->name('admin.users.edit');
Route::get('/admin-user-create', 'UserController@create')->name('admin.users.create');
Route::post('/admin-user-store', 'UserController@store')->name('admin.users.store');

Route::post('/acta-alta-medica-store', 'ActaAltaMedicaController@store')->name('store.alta');

Route::post('/acta-fallecimiento-store', 'ActaFallecidosController@store')->name('store.fallecimiento');

Route::get('/alta-medica/{id?}', 'ActaAltaMedicaController@index')->name('alta');
Route::get('/acta-fallecimiento/{id?}', 'ActaFallecidosController@index')->name('fallecimiento');
Auth::routes();

/*
define('STDIN',fopen("php://stdin","r"));
Route::get('install', function() {
    Artisan::call('migrate',['--force'=>true,'--seed'=>true]);
});*/

