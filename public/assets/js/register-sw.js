'use strict';

//const swReady = navigator.serviceWorker.ready;

document.addEventListener('DOMContentLoaded', function () {
    initSW();
});

function initSW() {
    if (!"serviceWorker" in navigator) {
        console.log("service worker isn't supported");
        return;
    }

    //don't use it here if you use service worker
    //for other stuff.
    if (!"PushManager" in window) {
        console.log("push isn't supported");
        return;
    }

    //register the service worker
    navigator.serviceWorker.register('https://cuimeca.com/sw.js')
        .then(() => {
             initPush();
        })
        .catch((err) => {
            console.log(err)
        });
}
